<?php

namespace common\modules\Xml\dataTypeAwareTag;

use common\modules\PurchaseEmulator\dto\request\CustomerInfoDto;
use common\modules\Xml\base\tag\AbstractTag;
use common\modules\Xml\base\tag\TagInterface;

abstract class AbstractCustomerInfoDtoTag extends AbstractTag
{
    /**
     * @var CustomerInfoDto
     */
    protected $data;

    public function setData($data): TagInterface
    {
        if (!$data instanceof CustomerInfoDto) {
            $this->throwInvalidDataTypeException($data, CustomerInfoDto::class);
        }

        return parent::setData($data);
    }
}
