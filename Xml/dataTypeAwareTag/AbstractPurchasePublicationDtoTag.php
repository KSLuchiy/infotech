<?php

namespace common\modules\Xml\dataTypeAwareTag;

use common\modules\PurchaseEmulator\dto\request\PurchasePublicationDto;
use common\modules\Xml\base\tag\AbstractTag;
use common\modules\Xml\base\tag\TagInterface;

abstract class AbstractPurchasePublicationDtoTag extends AbstractTag
{
    /**
     * @var PurchasePublicationDto
     */
    protected $data;

    public function setData($data): TagInterface
    {
        if (!$data instanceof PurchasePublicationDto) {
            $this->throwInvalidDataTypeException($data, PurchasePublicationDto::class);
        }

        return parent::setData($data);
    }
}
