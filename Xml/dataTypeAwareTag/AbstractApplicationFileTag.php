<?php

namespace common\modules\Xml\dataTypeAwareTag;

use common\models\Entity\Postgres\File;
use common\modules\Xml\base\tag\AbstractTag;
use common\modules\Xml\base\tag\TagInterface;

abstract class AbstractApplicationFileTag extends AbstractTag
{
    /**
     * @var File
     */
    protected $data;

    public function setData($data): TagInterface
    {
        if (!$data instanceof File) {
            $this->throwInvalidDataTypeException($data, File::class);
        }

        return parent::setData($data);
    }
}
