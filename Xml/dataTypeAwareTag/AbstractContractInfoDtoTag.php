<?php

namespace common\modules\Xml\dataTypeAwareTag;

use common\modules\PurchaseEmulator\dto\request\contractInfo\ContractInfoDto;
use common\modules\Xml\base\tag\AbstractTag;
use common\modules\Xml\base\tag\TagInterface;

abstract class AbstractContractInfoDtoTag extends AbstractTag
{
    /**
     * @var ContractInfoDto
     */
    protected $data;

    public function setData($data): TagInterface
    {
        if (!$data instanceof ContractInfoDto) {
            $this->throwInvalidDataTypeException($data, ContractInfoDto::class);
        }

        return parent::setData($data);
    }
}
