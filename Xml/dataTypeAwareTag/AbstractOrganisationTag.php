<?php

namespace common\modules\Xml\dataTypeAwareTag;

use common\models\Entity\Postgres\Organisation\Organisation;
use common\modules\Xml\base\tag\AbstractTag;
use common\modules\Xml\base\tag\TagInterface;

abstract class AbstractOrganisationTag extends AbstractTag
{
    /**
     * @var Organisation
     */
    protected $data;

    public function setData($data): TagInterface
    {
        if (!$data instanceof Organisation) {
            $this->throwInvalidDataTypeException($data, Organisation::class);
        }

        return parent::setData($data);
    }
}
