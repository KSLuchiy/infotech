<?php

namespace common\modules\Xml\dataTypeAwareTag;

use common\modules\Xml\base\tag\AbstractTag;
use common\modules\Xml\base\tag\TagInterface;

abstract class AbstractStringTag extends AbstractTag
{
    /**
     * @var string
     */
    protected $data;

    public function setData($data): TagInterface
    {
        if (!is_string($data)) {
            $this->throwInvalidDataTypeException($data, 'string');
        }

        return parent::setData($data);
    }
}
