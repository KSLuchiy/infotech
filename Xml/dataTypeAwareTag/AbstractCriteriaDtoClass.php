<?php

namespace common\modules\Xml\dataTypeAwareTag;

use common\modules\PurchaseEmulator\dto\request\criteria\CriteriaDto;
use common\modules\Xml\base\tag\AbstractTag;
use common\modules\Xml\base\tag\TagInterface;

abstract class AbstractCriteriaDtoClass extends AbstractTag
{
    /**
     * @var CriteriaDto
     */
    protected $data;

    public function setData($data): TagInterface
    {
        if (!$data instanceof CriteriaDto) {
            $this->throwInvalidDataTypeException($data, CriteriaDto::class);
        }

        return parent::setData($data);
    }
}
