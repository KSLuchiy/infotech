<?php

namespace common\modules\Xml\dataTypeAwareTag;

use common\Entity\Procedure\Contract;
use common\models\Entity\Postgres\Application;
use common\modules\Xml\base\tag\AbstractTag;
use common\modules\Xml\base\tag\TagInterface;

abstract class AbstractApplicationTag extends AbstractTag
{
    /**
     * @var Application
     */
    protected $data;

    public function setData($data): TagInterface
    {
        if (!$data instanceof Application) {
            $this->throwInvalidDataTypeException($data, Application::class);
        }

        return parent::setData($data);
    }
}
