<?php

namespace common\modules\Xml\dataTypeAwareTag;

use common\Entity\Procedure\Contract;
use common\modules\Xml\base\tag\AbstractTag;
use common\modules\Xml\base\tag\TagInterface;

abstract class AbstractContractTag extends AbstractTag
{
    /**
     * @var Contract
     */
    protected $data;

    public function setData($data): TagInterface
    {
        if (!$data instanceof Contract) {
            $this->throwInvalidDataTypeException($data, Contract::class);
        }

        return parent::setData($data);
    }
}
