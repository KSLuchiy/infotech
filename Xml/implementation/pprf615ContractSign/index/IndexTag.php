<?php

namespace common\modules\Xml\implementation\pprf615ContractSign\index;

use common\modules\Contract\service\Contract as ContractService;
use common\modules\Xml\base\provider\TagProviderInterface;
use common\modules\Xml\dataTypeAwareTag\AbstractContractTag;
use common\modules\Xml\interfaces\pprf615ContractSign\index\IndexTagInterface;
use DateTime;
use DateTimeZone;
use DOMDocument;
use DOMNode;
use Yii;
use yii\base\InvalidConfigException;
use function common\helpers\generateGuid;

class IndexTag extends AbstractContractTag implements IndexTagInterface
{
    /**
     * @var ContractService
     */
    private $contractService;

    /**
     * @throws InvalidConfigException
     */
    public function __construct(TagProviderInterface $tagProvider)
    {
        parent::__construct($tagProvider);

        $this->contractService = Yii::createObject(ContractService::class);
    }

    public function generate(DOMDocument $domTree): DOMNode
    {
        $index = $domTree->createElement('index');

        $id = $domTree->createElement('id', generateGuid());
        $index->appendChild($id);

        $sender = $domTree->createElement('sender', 'ETP_RAD');
        $index->appendChild($sender);

        $receiver = $domTree->createElement('receiver', 'OOC');
        $index->appendChild($receiver);

        $currentDateTimeUtc = (new DateTime())->setTimezone(new DateTimeZone('UTC'));
        $currentDateTimeUtcFormatted = $currentDateTimeUtc->format('Y-m-d\TH:i:s\Z');

        $createDateTime = $domTree->createElement('createDateTime', $currentDateTimeUtcFormatted);
        $index->appendChild($createDateTime);

        $objectType = $domTree->createElement('objectType', '615_CT');
        $index->appendChild($objectType);

        $objectId = $domTree->createElement('objectId', $this->contractService->generateContractNumber($this->data));
        $index->appendChild($objectId);

        $indexNum = $domTree->createElement('indexNum', 1);
        $index->appendChild($indexNum);

        $signature = $domTree->createElement('signature');
        $signature->setAttribute('type', 'CAdES-BES');
        $index->appendChild($signature);

        $mode = $domTree->createElement('mode', getenv('EIS_INTEGRATION_MODE'));
        $index->appendChild($mode);

        return $index;
    }
}
