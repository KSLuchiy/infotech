<?php

namespace common\modules\Xml\implementation\pprf615ContractSign;

use common\modules\Xml\dataTypeAwareTag\AbstractContractTag;
use common\modules\Xml\interfaces\pprf615ContractSign\data\DataTagInterface;
use common\modules\Xml\interfaces\pprf615ContractSign\index\IndexTagInterface;
use common\modules\Xml\interfaces\pprf615ContractSign\Pprf615ContractSignTagInterface;
use DOMDocument;
use DOMNode;

class Pprf615ContractSignTag extends AbstractContractTag implements Pprf615ContractSignTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $pprf615ContractSign = $domTree->createElement('pprf615ContractSign');
        $pprf615ContractSign->setAttribute('xmlns', 'http://zakupki.gov.ru/oos/integration/1');

        $indexTag = $this->tagProvider->getTag(IndexTagInterface::class);
        $pprf615ContractSign->appendChild($indexTag->setData($this->data)->generate($domTree));

        $dataTag = $this->tagProvider->getTag(DataTagInterface::class);
        $pprf615ContractSign->appendChild($dataTag->setData($this->data)->generate($domTree));

        return $pprf615ContractSign;
    }
}
