<?php

namespace common\modules\Xml\implementation\pprf615ContractSign\data\attachmentsInfo;

use common\Entity\Procedure\Document\Contract\ContractDocument;
use common\modules\Xml\base\tag\AbstractTag;
use common\modules\Xml\base\tag\TagInterface;
use common\modules\Xml\interfaces\pprf615ContractSign\data\attachmentsInfo\attachmentInfo\AttachmentInfoTagInterface;
use common\modules\Xml\interfaces\pprf615ContractSign\data\attachmentsInfo\AttachmentsInfoTagInterface;
use DOMDocument;
use DOMNode;

class AttachmentsInfoTag extends AbstractTag implements AttachmentsInfoTagInterface
{
    /**
     * @var ContractDocument[]
     */
    protected $data;

    public function setData($data): TagInterface
    {
        if (!is_array($data) || !reset($data) instanceof ContractDocument) {
            $this->throwInvalidDataTypeException($data, 'ContractDocument[]');
        }

        return parent::setData($data);
    }

    public function generate(DOMDocument $domTree): DOMNode
    {
        $attachmentsInfo = $domTree->createElement('attachmentsInfo');
        $attachmentsInfo->setAttribute('xmlns', 'http://zakupki.gov.ru/oos/pprf615types/1');

        foreach ($this->data as $document) {
            $attachmentInfoTag = $this->tagProvider->getTag(AttachmentInfoTagInterface::class);
            $attachmentsInfo->appendChild($attachmentInfoTag->setData($document)->generate($domTree));
        }

        return $attachmentsInfo;
    }
}
