<?php

namespace common\modules\Xml\implementation\pprf615ContractSign\data\attachmentsInfo\attachmentInfo\cryptoSigns;

use common\Entity\Organization\Signature;
use common\Exception\RuntimeException;
use common\modules\Xml\base\tag\AbstractTag;
use common\modules\Xml\base\tag\TagInterface;
use common\modules\Xml\interfaces\pprf615ContractSign\data\attachmentsInfo\attachmentInfo\cryptoSigns\CryptoSignsTagInterface;
use DOMDocument;
use DOMNode;
use function common\helpers\normalizePath;

class CryptoSignsTag extends AbstractTag implements CryptoSignsTagInterface
{
    /**
     * @var array
     */
    protected $data;

    public function setData($data): TagInterface
    {
        if (!is_array($data) || !array_key_exists('customer', $data) || !array_key_exists('supplier', $data)) {
            $this->throwInvalidDataTypeException($data, 'Signature[]');
        }

        return parent::setData($data);
    }

    public function generate(DOMDocument $domTree): DOMNode
    {
        $cryptoSigns = $domTree->createElement('cryptoSigns');

        /**
         * @var Signature|null $sign
         */
        foreach ($this->data as $key => $sign) {
            if (!$sign instanceof Signature) {
                continue;
            }
            $normalizedPath = normalizePath($sign->getSignedDataUri());
            if (!file_exists($normalizedPath)) {
                throw new RuntimeException(sprintf('Не найден файл подписи по пути %s для подписи с id = %s', $normalizedPath, $sign->getId()));
            }

            $signature = $domTree->createElement('signature', base64_encode(file_get_contents($normalizedPath)));
            $signature->setAttribute('type', $key === 'supplier' ? 'CAdES-BES' : 'CAdES-A');

            $cryptoSigns->appendChild($signature);
        }

        return $cryptoSigns;
    }
}
