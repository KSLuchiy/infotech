<?php

namespace common\modules\Xml\implementation\pprf615ContractSign\data\attachmentsInfo\attachmentInfo;

use common\Entity\Procedure\Document\Contract\ContractDocument;
use common\Exception\RuntimeException;
use common\modules\Xml\base\tag\AbstractTag;
use common\modules\Xml\base\tag\TagInterface;
use common\modules\Xml\interfaces\pprf615ContractSign\data\attachmentsInfo\attachmentInfo\AttachmentInfoTagInterface;
use common\modules\Xml\interfaces\pprf615ContractSign\data\attachmentsInfo\attachmentInfo\cryptoSigns\CryptoSignsTagInterface;
use DOMDocument;
use DOMNode;
use function common\helpers\normalizePath;

class AttachmentInfoTag extends AbstractTag implements AttachmentInfoTagInterface
{
    /**
     * @var ContractDocument
     */
    protected $data;

    public function setData($data): TagInterface
    {
        if (!$data instanceof ContractDocument) {
            $this->throwInvalidDataTypeException($data, ContractDocument::class);
        }

        return parent::setData($data);
    }

    public function generate(DOMDocument $domTree): DOMNode
    {
        $attachmentInfo = $domTree->createElement('attachmentInfo');

        $fileName = $domTree->createElement('fileName', $this->data->getRealName());
        $attachmentInfo->appendChild($fileName);

        $docDate = $domTree->createElement('docDate', $this->data->getCreateDateTime()->format('c'));
        $attachmentInfo->appendChild($docDate);

        $normalizedPath = normalizePath($this->data->getUri());
        if (!file_exists($normalizedPath)) {
            throw new RuntimeException(sprintf('Не найден файл контракта по пути %s для контракта с id = %s', $normalizedPath, $this->data->getId()));
        }
        $base64Content = base64_encode(file_get_contents($normalizedPath));
        $content = $domTree->createElement('content', $base64Content);
        $attachmentInfo->appendChild($content);

        $cryptoSignsTag = $this->tagProvider->getTag(CryptoSignsTagInterface::class);
        $attachmentInfo->appendChild($cryptoSignsTag->setData(['customer' => $this->data->getContract()->getCustomerSignature(), 'supplier' => $this->data->getContract()->getSupplierSignatureId()])->generate($domTree));

        return $attachmentInfo;
    }
}
