<?php

namespace common\modules\Xml\implementation\pprf615ContractSign\data\contractorInfo;

use common\components\App;
use common\Exception\InvalidArgumentException;
use common\Exception\RuntimeException;
use common\models\Entity\Postgres\Organisation\Organisation;
use common\models\Entity\Postgres\Organisation\OrganisationRefRole;
use common\modules\Xml\base\provider\TagProviderInterface;
use common\modules\Xml\dataTypeAwareTag\AbstractContractTag;
use common\modules\Xml\interfaces\pprf615ContractSign\data\contractorInfo\ContractorInfoTagInterface;
use common\modules\Xml\interfaces\pprf615ContractSign\data\contractorInfo\individualPersonInfo\IndividualPersonInfoTagInterface;
use common\modules\Xml\interfaces\pprf615ContractSign\data\contractorInfo\legalEntityForeignStateInfo\LegalEntityForeignStateInfoTagInterface;
use common\modules\Xml\interfaces\pprf615ContractSign\data\contractorInfo\legalEntityRfInfo\LegalEntityRfInfoTagInterface;
use common\Repository\Postgres\Organisation\OrganisationRepository;
use Doctrine\ORM\EntityManagerInterface;
use DOMDocument;
use DOMNode;

class ContractorInfoTag extends AbstractContractTag implements ContractorInfoTagInterface
{
    private const ROLE_TO_TAG_CLASS = [
        OrganisationRefRole::ROLE_SUPPLIER_IP_FOREIGN => IndividualPersonInfoTagInterface::class,
        OrganisationRefRole::ROLE_SUPPLIER_IP => IndividualPersonInfoTagInterface::class,
        OrganisationRefRole::ROLE_SUPPLIER_CORPORATE => LegalEntityRfInfoTagInterface::class,
        OrganisationRefRole::ROLE_SUPPLIER_CORPORATE_FOREIGN => LegalEntityForeignStateInfoTagInterface::class,
    ];

    /**
     * @var OrganisationRepository
     */
    private $organisationRepository;

    public function __construct(TagProviderInterface $tagProvider)
    {
        parent::__construct($tagProvider);

        /** @var EntityManagerInterface $entityManagerEtp */
        $entityManagerEtp = App::doctrine_pgsql()->getEntityManager();
        $this->organisationRepository = $entityManagerEtp->getRepository(Organisation::class);
    }

    public function generate(DOMDocument $domTree): DOMNode
    {
        $contractorInfo = $domTree->createElement('contractorInfo');
        $contractorInfo->setAttribute('xmlns', 'http://zakupki.gov.ru/oos/pprf615types/1');

        $supplierSectionks = $this->data->getSupplier();
        $supplierEtp = $this->organisationRepository->findOneByGuid($supplierSectionks->getGuid());
        if ($supplierEtp === null) {
            throw new InvalidArgumentException('Не найден участник в БД etp с guid = ' . $supplierSectionks->getGuid());
        }

        $type = $supplierEtp->getOrganizationType();
        if ($type === null) {
            throw new RuntimeException('Нет ролей у организации с guid = ' . $supplierEtp->getGuid());
        }

        $tagClass = self::ROLE_TO_TAG_CLASS[$type->getCode()] ?? null;
        if ($tagClass === null) {
            throw new RuntimeException(sprintf('Не найден тег для роли %s для организации с guid = %s', $type->getCode(), $supplierEtp->getGuid()));
        }

        $supplierInfoTag = $this->tagProvider->getTag($tagClass);
        $contractorInfo->appendChild($supplierInfoTag->setData($supplierEtp)->generate($domTree));

        return $contractorInfo;
    }
}
