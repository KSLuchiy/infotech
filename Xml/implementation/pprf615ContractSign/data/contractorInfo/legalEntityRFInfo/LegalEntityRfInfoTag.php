<?php

namespace common\modules\Xml\implementation\pprf615ContractSign\data\contractorInfo\legalEntityRFInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractOrganisationTag;
use common\modules\Xml\interfaces\pprf615ContractSign\data\contractorInfo\legalEntityRfInfo\contactPerson\ContactPersonTagInterface;
use common\modules\Xml\interfaces\pprf615ContractSign\data\contractorInfo\legalEntityRfInfo\LegalEntityRfInfoTagInterface;
use DOMDocument;
use DOMNode;

class LegalEntityRfInfoTag extends AbstractOrganisationTag implements LegalEntityRfInfoTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $legalEntityRfInfo = $domTree->createElement('legalEntityRFInfo');

        $fullName = $domTree->createElement('fullName', $this->data->getFullName());
        $legalEntityRfInfo->appendChild($fullName);

        $inn = $domTree->createElement('INN', $this->data->getInn());
        $legalEntityRfInfo->appendChild($inn);

        $kpp = $domTree->createElement('KPP', $this->data->getKpp());
        $legalEntityRfInfo->appendChild($kpp);

        $orgFactAddress = $domTree->createElement('orgFactAddress', $this->data->getFactualAddress()->getAddressLine());
        $legalEntityRfInfo->appendChild($orgFactAddress);

        if ($this->data->getShortName() !== null) {
            $shorName = $domTree->createElement('shortName', $this->data->getShortName());
            $legalEntityRfInfo->appendChild($shorName);
        }

        if ($this->data->getNalogRegistrationDate()) {
            $registrationDate = $domTree->createElement('registrationDate', $this->data->getNalogRegistrationDate()->format('Y-m-d'));
            $legalEntityRfInfo->appendChild($registrationDate);
        }

        if ($this->data->getEmail() !== null) {
            $email = $domTree->createElement('eMail', $this->data->getEmail());
            $legalEntityRfInfo->appendChild($email);
        }

        if ($this->data->getContactLastName() !== null && $this->data->getContactFirstName() !== null) {
            $contactPersonTag = $this->tagProvider->getTag(ContactPersonTagInterface::class);
            $legalEntityRfInfo->appendChild($contactPersonTag->setData($this->data)->generate($domTree));
        }

        if ($this->data->getPhone() !== null) {
            $phone = $domTree->createElement('phone', $this->data->getPhone());
            $legalEntityRfInfo->appendChild($phone);
        }

        return $legalEntityRfInfo;
    }
}
