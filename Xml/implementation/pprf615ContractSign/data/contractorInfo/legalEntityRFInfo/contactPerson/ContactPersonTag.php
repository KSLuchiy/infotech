<?php

namespace common\modules\Xml\implementation\pprf615ContractSign\data\contractorInfo\legalEntityRFInfo\contactPerson;

use common\modules\Xml\dataTypeAwareTag\AbstractOrganisationTag;
use common\modules\Xml\interfaces\pprf615ContractSign\data\contractorInfo\legalEntityRfInfo\contactPerson\ContactPersonTagInterface;
use DOMDocument;
use DOMNode;

class ContactPersonTag extends AbstractOrganisationTag implements ContactPersonTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $contactPerson = $domTree->createElement('contactPerson');

        $lastName = $domTree->createElement('lastName', $this->data->getContactLastName());
        $contactPerson->appendChild($lastName);

        $firstName = $domTree->createElement('firstName', $this->data->getContactFirstName());
        $contactPerson->appendChild($firstName);

        if ($this->data->getContactMiddleName() !== null) {
            $middleName = $domTree->createElement('middleName', $this->data->getContactMiddleName());
            $contactPerson->appendChild($middleName);
        }

        return $contactPerson;
    }
}
