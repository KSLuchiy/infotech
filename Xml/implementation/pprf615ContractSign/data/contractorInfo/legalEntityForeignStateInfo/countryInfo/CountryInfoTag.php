<?php

namespace common\modules\Xml\implementation\pprf615ContractSign\data\contractorInfo\legalEntityForeignStateInfo\countryInfo;

use common\components\App;
use common\Exception\RuntimeException;
use common\models\Entity\Postgres\Organization;
use common\modules\Xml\base\provider\TagProviderInterface;
use common\modules\Xml\dataTypeAwareTag\AbstractStringTag;
use common\Repository\Postgres\OrganizationRepository;
use Doctrine\ORM\EntityManagerInterface;
use DOMDocument;
use DOMNode;

class CountryInfoTag extends AbstractStringTag
{
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;

    public function __construct(TagProviderInterface $tagProvider)
    {
        parent::__construct($tagProvider);

        /** @var EntityManagerInterface $entityManagerProcedures */
        $entityManagerProcedures = App::doctrine_pgsql_procedures()->getEntityManager();
        $this->organizationRepository = $entityManagerProcedures->getRepository(Organization::class);
    }

    public function generate(DOMDocument $domTree): DOMNode
    {
        $organization = $this->organizationRepository->findOneByGuid($this->data);
        if ($organization === null) {
            throw new RuntimeException('В БД procedures не найдена организация с guid = ' . $this->data);
        }

        $countryInfo = $domTree->createElement('countryInfo');

        $countryCode = $domTree->createElement('countryCode', $organization->getCountryCode());
        $countryInfo->appendChild($countryCode);

        $countryFullName = $domTree->createElement('countryFullName', $organization->getCountry());
        $countryInfo->appendChild($countryFullName);

        return $countryInfo;
    }
}
