<?php

namespace common\modules\Xml\implementation\pprf615ContractSign\data\contractorInfo\legalEntityForeignStateInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractOrganisationTag;
use common\modules\Xml\interfaces\pprf615ContractSign\data\contractorInfo\legalEntityForeignStateInfo\countyInfo\CountryInfoTagInterface;
use common\modules\Xml\interfaces\pprf615ContractSign\data\contractorInfo\legalEntityForeignStateInfo\LegalEntityForeignStateInfoTagInterface;
use DOMDocument;
use DOMNode;

class LegaEntityForeignStateInfoTag extends AbstractOrganisationTag implements LegalEntityForeignStateInfoTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $legalEntityForeignStateInfo = $domTree->createElement('legalEntityForeignStateInfo');

        $fullName = $domTree->createElement('fullName', $this->data->getFullName());
        $legalEntityForeignStateInfo->appendChild($fullName);

        if ($this->data->getFullLatName() !== null) {
            $fullNameLat = $domTree->createElement('fullNameLat', $this->data->getFullLatName());
            $legalEntityForeignStateInfo->appendChild($fullNameLat);
        }

        if ($this->data->getInn() !== null) {
            $inn = $domTree->createElement('INN', $this->data->getInn());
            $legalEntityForeignStateInfo->appendChild($inn);
        }

        if ($this->data->getKpp() !== null) {
            $kpp = $domTree->createElement('KPP', $this->data->getKpp());
            $legalEntityForeignStateInfo->appendChild($kpp);
        }

        $countryInfoTag = $this->tagProvider->getTag(CountryInfoTagInterface::class);
        $legalEntityForeignStateInfo->appendChild($countryInfoTag->setData($this->data->getGuid())->generate($domTree));

        $placeOfStayInRf = $domTree->createElement('placeOfStayInRF', $this->data->getFactualAddress()->getAddressLine());
        $legalEntityForeignStateInfo->appendChild($placeOfStayInRf);

        if ($this->data->getNalogRegistrationDate()) {
            $registrationDate = $domTree->createElement($this->data->getNalogRegistrationDate()->format('Y-m-d'));
            $legalEntityForeignStateInfo->appendChild($registrationDate);
        }

        if ($this->data->getEmail() !== null) {
            $email = $domTree->createElement('eMail', $this->data->getEmail());
            $legalEntityForeignStateInfo->appendChild($email);
        }

        if ($this->data->getPhone() !== null) {
            $phone = $domTree->createElement('phone', $this->data->getPhone());
            $legalEntityForeignStateInfo->appendChild($phone);
        }

        return $legalEntityForeignStateInfo;
    }
}
