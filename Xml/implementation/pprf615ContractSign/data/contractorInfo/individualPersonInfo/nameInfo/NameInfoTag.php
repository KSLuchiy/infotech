<?php

namespace common\modules\Xml\implementation\pprf615ContractSign\data\contractorInfo\individualPersonInfo\nameInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractOrganisationTag;
use common\modules\Xml\interfaces\pprf615ContractSign\data\contractorInfo\individualPersonInfo\nameInfo\NameInfoTagInterface;
use DOMDocument;
use DOMNode;

class NameInfoTag extends AbstractOrganisationTag implements NameInfoTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $nameInfo = $domTree->createElement('nameInfo');

        $lastName = $domTree->createElement('lastName', $this->data->getContactLastName());
        $nameInfo->appendChild($lastName);

        $firstName = $domTree->createElement('firstName', $this->data->getContactFirstName());
        $nameInfo->appendChild($firstName);

        if ($this->data->getContactMiddleName() !== null) {
            $middleName = $domTree->createElement('middleName', $this->data->getContactMiddleName());
            $nameInfo->appendChild($middleName);
        }

        return $nameInfo;
    }
}
