<?php

namespace common\modules\Xml\implementation\pprf615ContractSign\data\contractorInfo\individualPersonInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractOrganisationTag;
use common\modules\Xml\interfaces\pprf615ContractSign\data\contractorInfo\individualPersonInfo\IndividualPersonInfoTagInterface;
use common\modules\Xml\interfaces\pprf615ContractSign\data\contractorInfo\individualPersonInfo\nameInfo\NameInfoTagInterface;
use DOMDocument;
use DOMNode;

class IndividualPersonInfoTag extends AbstractOrganisationTag implements IndividualPersonInfoTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $individualPersonInfo = $domTree->createElement('individualPersonInfo');

        $nameInfoTag = $this->tagProvider->getTag(NameInfoTagInterface::class);
        $individualPersonInfo->appendChild($nameInfoTag->setData($this->data)->generate($domTree));

        $inn = $domTree->createElement('INN', $this->data->getInn());
        $individualPersonInfo->appendChild($inn);

        $factAddress = $domTree->createElement('factAddress', $this->data->getFactualAddress()->getAddressLine());
        $individualPersonInfo->appendChild($factAddress);

        if ($this->data->getNalogRegistrationDate()) {
            $registrationDate = $domTree->createElement('registrationDate', $this->data->getNalogRegistrationDate()->format('Y-m-d'));
            $individualPersonInfo->appendChild($registrationDate);
        }

        if ($this->data->getEmail() !== null) {
            $email = $domTree->createElement('eMail', $this->data->getEmail());
            $individualPersonInfo->appendChild($email);
        }

        if ($this->data->getPhone() !== null) {
            $phone = $domTree->createElement('phone', $this->data->getPhone());
            $individualPersonInfo->appendChild($phone);
        }

        return $individualPersonInfo;
    }
}
