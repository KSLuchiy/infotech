<?php

namespace common\modules\Xml\implementation\pprf615ContractSign\data;

use common\Entity\Procedure\Document\Contract\Contract;
use common\Entity\Procedure\Document\Contract\ContractDocument;
use common\Entity\Procedure\Document\Contract\Customer;
use common\Entity\Procedure\Document\Contract\ExecutionRequirement;
use common\Entity\Procedure\Document\Contract\Supplier;
use common\modules\Xml\dataTypeAwareTag\AbstractContractTag;
use common\modules\Xml\interfaces\pprf615ContractSign\data\attachmentsInfo\AttachmentsInfoTagInterface;
use common\modules\Xml\interfaces\pprf615ContractSign\data\commonInfo\CommonInfoTagInterface;
use common\modules\Xml\interfaces\pprf615ContractSign\data\contractorInfo\ContractorInfoTagInterface;
use common\modules\Xml\interfaces\pprf615ContractSign\data\customerInfo\CustomerInfoTagInterface;
use common\modules\Xml\interfaces\pprf615ContractSign\data\DataTagInterface;
use common\modules\Xml\interfaces\pprf615ContractSign\data\extPrintFormInfo\ExtPrintFormInfoTagInterface;
use common\modules\Xml\interfaces\pprf615ContractSign\data\financesInfo\FinancesInfoTagInterface;
use DOMDocument;
use DOMNode;

class DataTag extends AbstractContractTag implements DataTagInterface
{
    private const CHILD_TAGS = [
        CommonInfoTagInterface::class,
        CustomerInfoTagInterface::class,
        ContractorInfoTagInterface::class,
        FinancesInfoTagInterface::class,
        ExtPrintFormInfoTagInterface::class,
    ];

    private const INCLUDED_DOCUMENT_CLASSES = [
        ExecutionRequirement::class,
        Supplier::class,
        Customer::class,
        Contract::class,
    ];

    public function generate(DOMDocument $domTree): DOMNode
    {
        $data = $domTree->createElement('data');
        $data->setAttribute('schemeVersion', '1.0');

        foreach (self::CHILD_TAGS as $tagInterface) {
            $tag = $this->tagProvider->getTag($tagInterface);
            $data->appendChild($tag->setData($this->data)->generate($domTree));
        }

        $contractDocuments = $this->getAdditionalContractDocuments();
        if (!empty($contractDocuments)) {
            $attachmentsInfoTag = $this->tagProvider->getTag(AttachmentsInfoTagInterface::class);
            $data->appendChild($attachmentsInfoTag->setData($contractDocuments)->generate($domTree));
        }

        return $data;
    }

    /**
     * @return ContractDocument[] Документы, приложенные к контракту
     */
    private function getAdditionalContractDocuments(): array
    {
        return $this->data->getDocuments()->filter(static function (ContractDocument $contractDocument) {
            return in_array(get_class($contractDocument), self::INCLUDED_DOCUMENT_CLASSES, true);
        })->toArray();
    }
}
