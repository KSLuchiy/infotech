<?php

namespace common\modules\Xml\implementation\pprf615ContractSign\data\extPrintFormInfo;

use common\Exception\RuntimeException;
use common\modules\Xml\dataTypeAwareTag\AbstractContractTag;
use common\modules\Xml\interfaces\pprf615ContractSign\data\extPrintFormInfo\ExtPrintFormInfoTagInterface;
use DOMDocument;
use DOMNode;
use function common\helpers\normalizePath;

class ExtPrintFormInfoTag extends AbstractContractTag implements ExtPrintFormInfoTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $contractDocument = $this->data->getContractDocument();
        $normalizedPath = normalizePath($contractDocument->getUri());

        if (!file_exists($normalizedPath)) {
            throw new RuntimeException(sprintf('Не найден файл контракта по пути %s для контракта с id = %s', $normalizedPath, $this->data->getId()));
        }

        $extPrintFormInfo = $domTree->createElement('extPrintFormInfo');
        $extPrintFormInfo->setAttribute('xmlns', 'http://zakupki.gov.ru/oos/pprf615types/1');

        $base64Content = base64_encode(file_get_contents($normalizedPath));
        $content = $domTree->createElement('content', $base64Content);
        $extPrintFormInfo->appendChild($content);

        $customerSignature = $this->data->getCustomerContractFileSignId();
        $normalizedSignaturePath = normalizePath($customerSignature->getStringDataUri());

        if (!file_exists($normalizedSignaturePath)) {
            throw new RuntimeException(sprintf('Не найден файл подписи по пути %s для подписи с id = %s', $normalizedSignaturePath, $customerSignature->getId()));
        }

        $signature = $domTree->createElement('signature', base64_encode(file_get_contents($normalizedSignaturePath)));
        $signature->setAttribute('type', 'CAdES-BES');
        $extPrintFormInfo->appendChild($signature);

        $fileType = $domTree->createElement('fileType', $contractDocument->getExtension());
        $extPrintFormInfo->appendChild($fileType);

        return $extPrintFormInfo;
    }
}
