<?php

namespace common\modules\Xml\implementation\pprf615ContractSign\data\commonInfo;

use common\modules\Contract\service\Contract as ContractService;
use common\modules\Xml\base\provider\TagProviderInterface;
use common\modules\Xml\dataTypeAwareTag\AbstractContractTag;
use common\modules\Xml\interfaces\pprf615ContractSign\data\commonInfo\CommonInfoTagInterface;
use common\modules\Xml\interfaces\pprf615ContractSign\data\commonInfo\purchaseInfo\PurchaseInfoTagInterface;
use DOMDocument;
use DOMNode;
use Yii;
use yii\base\InvalidConfigException;

class CommonInfoTag extends AbstractContractTag implements CommonInfoTagInterface
{
    /**
     * @var ContractService
     */
    private $contractService;

    /**
     * @throws InvalidConfigException
     */
    public function __construct(TagProviderInterface $tagProvider)
    {
        parent::__construct($tagProvider);

        $this->contractService = Yii::createObject(ContractService::class);
    }

    public function generate(DOMDocument $domTree): DOMNode
    {
        $commonInfo = $domTree->createElement('commonInfo');
        $commonInfo->setAttribute('xmlns', 'http://zakupki.gov.ru/oos/pprf615types/1');

        $purchaseInfoTag = $this->tagProvider->getTag(PurchaseInfoTagInterface::class);
        $commonInfo->appendChild($purchaseInfoTag->setData($this->data)->generate($domTree));

        $contractNumber = $domTree->createElement('contractNumber', $this->contractService->generateContractNumber($this->data));
        $commonInfo->appendChild($contractNumber);

        $signDateValue = $this->data->getSignedFromDateTime()->format('Y-m-d');
        $signDate = $domTree->createElement('signDate', $signDateValue);
        $commonInfo->appendChild($signDate);

        return $commonInfo;
    }
}
