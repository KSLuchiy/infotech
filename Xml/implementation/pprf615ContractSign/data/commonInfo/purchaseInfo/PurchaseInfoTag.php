<?php

namespace common\modules\Xml\implementation\pprf615ContractSign\data\commonInfo\purchaseInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractContractTag;
use common\modules\Xml\interfaces\pprf615ContractSign\data\commonInfo\purchaseInfo\PurchaseInfoTagInterface;
use DOMDocument;
use DOMNode;

class PurchaseInfoTag extends AbstractContractTag implements PurchaseInfoTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $purchaseInfo = $domTree->createElement('purchaseInfo');

        $purchaseNumber = $domTree->createElement('purchaseNumber', $this->data->getProcedure()->getPurchaseNumber());
        $purchaseInfo->appendChild($purchaseNumber);

        return $purchaseInfo;
    }
}
