<?php

namespace common\modules\Xml\implementation\pprf615ContractSign\data\financesInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractContractTag;
use common\modules\Xml\interfaces\pprf615ContractSign\data\financesInfo\FinancesInfoTagInterface;
use DOMDocument;
use DOMNode;

class FinancesInfoTag extends AbstractContractTag implements FinancesInfoTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $financesInfo = $domTree->createElement('financesInfo');
        $financesInfo->setAttribute('xmlns', 'http://zakupki.gov.ru/oos/pprf615types/1');

        $price = $domTree->createElement('price', bcmul($this->data->getPrice(), '1', 2));
        $financesInfo->appendChild($price);

        return $financesInfo;
    }
}
