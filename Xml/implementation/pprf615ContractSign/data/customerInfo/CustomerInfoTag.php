<?php

namespace common\modules\Xml\implementation\pprf615ContractSign\data\customerInfo;

use common\components\App;
use common\Exception\InvalidArgumentException;
use common\models\Entity\Postgres\Organisation\Organisation;
use common\modules\Xml\base\provider\TagProviderInterface;
use common\modules\Xml\dataTypeAwareTag\AbstractContractTag;
use common\modules\Xml\interfaces\pprf615ContractSign\data\customerInfo\CustomerInfoTagInterface;
use common\Repository\Postgres\Organisation\OrganisationRepository;
use Doctrine\ORM\EntityManagerInterface;
use DOMDocument;
use DOMNode;

class CustomerInfoTag extends AbstractContractTag implements CustomerInfoTagInterface
{
    /**
     * @var OrganisationRepository
     */
    private $organisationRepository;

    public function __construct(TagProviderInterface $tagProvider)
    {
        parent::__construct($tagProvider);

        /** @var EntityManagerInterface $entityManagerEtp */
        $entityManagerEtp = App::doctrine_pgsql()->getEntityManager();
        $this->organisationRepository = $entityManagerEtp->getRepository(Organisation::class);
    }

    public function generate(DOMDocument $domTree): DOMNode
    {
        $sectionksOrganisation = $this->data->getCustomer();
        $etpOrganisation = $this->organisationRepository->findOneByGuid($sectionksOrganisation->getGuid());
        if ($etpOrganisation === null) {
            throw new InvalidArgumentException('В БД etp не найдена организация с guid = ' . $sectionksOrganisation->getGuid());
        }

        $customerInfo = $domTree->createElement('customerInfo');
        $customerInfo->setAttribute('xmlns', 'http://zakupki.gov.ru/oos/pprf615types/1');

        $regNum = $domTree->createElement('regNum', $etpOrganisation->getRegNumber());
        $customerInfo->appendChild($regNum);

        if ($etpOrganisation->getConsRegistryNum() !== null) {
            $consRegistryNum = $domTree->createElement('consRegistryNum', $etpOrganisation->getConsRegistryNum());
            $customerInfo->appendChild($consRegistryNum);
        }

        $fullName = $domTree->createElement('fullName', $etpOrganisation->getFullName());
        $customerInfo->appendChild($fullName);

        $shortName = $domTree->createElement('shortName', $etpOrganisation->getShortName());
        $customerInfo->appendChild($shortName);

        $inn = $domTree->createElement('INN', $etpOrganisation->getInn());
        $customerInfo->appendChild($inn);

        $kpp = $domTree->createElement('KPP', $etpOrganisation->getKpp());
        $customerInfo->appendChild($kpp);

        return $customerInfo;
    }
}
