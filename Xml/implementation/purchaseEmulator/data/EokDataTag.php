<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data;

use DOMDocument;
use DOMElement;
use DOMException;

class EokDataTag extends AbstractDataTag
{
    /**
     * @throws DOMException
     */
    protected function generateDataTag(DOMDocument $domTree): DOMElement
    {
        $data = $domTree->createElement('integration:data');
        $data->setAttribute('schemeVersion', '11.3');
        return $data;
    }

    protected function getId(): string
    {
        return 93152;
    }
}
