<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\flags;

use common\modules\Xml\base\tag\AbstractFromXmlTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\flags\FlagsTagInterface;


class EokPsdFlagsTag extends AbstractFromXmlTag implements FlagsTagInterface
{
    protected function getPathToXml(): string
    {
        return 'purchaseEmulator/eokPsd/flags.xml';
    }
}
