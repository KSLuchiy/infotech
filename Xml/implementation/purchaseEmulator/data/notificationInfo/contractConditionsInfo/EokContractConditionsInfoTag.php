<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\contractConditionsInfo;

class EokContractConditionsInfoTag extends ContractConditionsInfoTag
{
    protected function needAdditionalInfo(): bool
    {
        return true;
    }
}
