<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\contractConditionsInfo\maxPriceInfo\currency;


use common\modules\Xml\base\tag\AbstractFromXmlTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\contractConditionsInfo\maxPriceInfo\currency\CurrencyTagInterface;


class CurrencyTag extends AbstractFromXmlTag implements CurrencyTagInterface
{
    protected function getPathToXml(): string
    {
        return 'purchaseEmulator/common/currency.xml';
    }
}
