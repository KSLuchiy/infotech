<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\contractConditionsInfo\maxPriceInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractPurchasePublicationDtoTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\contractConditionsInfo\maxPriceInfo\currency\CurrencyTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\contractConditionsInfo\maxPriceInfo\MaxPriceInfoTagInterface;
use DOMDocument;
use DOMNode;

class MaxPriceInfoTag extends AbstractPurchasePublicationDtoTag implements MaxPriceInfoTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $maxPriceInfo = $domTree->createElement('EPtypes:maxPriceInfo');

        $maxPrice = $domTree->createElement('EPtypes:maxPrice', mb_ereg_replace(' ', '', $this->data->getInitialPrice()));
        $maxPriceInfo->appendChild($maxPrice);

        $currencyTag = $this->tagProvider->getTag(CurrencyTagInterface::class);
        $maxPriceInfo->appendChild($currencyTag->setData($this->data)->generate($domTree));

        return $maxPriceInfo;
    }
}
