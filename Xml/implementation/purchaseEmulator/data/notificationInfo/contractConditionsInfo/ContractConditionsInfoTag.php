<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\contractConditionsInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractPurchasePublicationDtoTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\contractConditionsInfo\ContractConditionsInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\contractConditionsInfo\contractMultiInfo\ContractMultiInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\contractConditionsInfo\maxPriceInfo\MaxPriceInfoTagInterface;
use DOMDocument;
use DOMNode;

class ContractConditionsInfoTag extends AbstractPurchasePublicationDtoTag implements ContractConditionsInfoTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $contractConditionsInfo = $domTree->createElement('EPtypes:contractConditionsInfo');

        $maxPriceInfo = $this->tagProvider->getTag(MaxPriceInfoTagInterface::class);
        $contractConditionsInfo->appendChild($maxPriceInfo->setData($this->data)->generate($domTree));

        if($this->needAdditionalInfo()) {
            $standardContractNumber = $domTree->createElement('EPtypes:standardContractNumber', '0190600000121003');
            $contractConditionsInfo->appendChild($standardContractNumber);

            $contractMultiInfo = $this->tagProvider->getTag(ContractMultiInfoTagInterface::class);
            $contractConditionsInfo->appendChild($contractMultiInfo->setData($this->data)->generate($domTree));
        }

        return $contractConditionsInfo;
    }

    protected function needAdditionalInfo(): bool
    {
        return false;
    }
}
