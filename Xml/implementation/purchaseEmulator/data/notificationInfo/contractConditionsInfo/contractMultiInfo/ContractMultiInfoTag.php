<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\contractConditionsInfo\contractMultiInfo;

use common\modules\Xml\base\tag\AbstractFromXmlTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\contractConditionsInfo\contractMultiInfo\ContractMultiInfoTagInterface;

class ContractMultiInfoTag extends AbstractFromXmlTag implements ContractMultiInfoTagInterface
{
    protected function getPathToXml(): string
    {
        return 'purchaseEmulator/common/contractMultiInfo.xml';
    }
}
