<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\restrictionsInfo;

use common\modules\Xml\base\tag\AbstractTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\restrictionsInfo\restrictionInfo\RestrictionInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\restrictionsInfo\RestrictionsInfoTagInterface;
use DOMDocument;
use DOMNode;

class RestrictionsInfoTag extends AbstractTag implements RestrictionsInfoTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $restrictionsInfo = $domTree->createElement('EPtypes:restrictionsInfo');

        foreach ($this->data->getRestrictionsInfo() as $restriction) {
            $restrictionInfo = $this->tagProvider->getTag(RestrictionInfoTagInterface::class);
            $restrictionsInfo->appendChild($restrictionInfo->setData($restriction)->generate($domTree));
        }
        return $restrictionsInfo;
    }
}
