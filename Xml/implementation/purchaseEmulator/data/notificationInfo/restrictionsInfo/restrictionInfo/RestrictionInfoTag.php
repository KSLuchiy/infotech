<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\restrictionsInfo\restrictionInfo;

use common\modules\PurchaseEmulator\dto\request\restriction\RestrictionDto;
use common\modules\Xml\base\tag\AbstractTag;
use common\modules\Xml\base\tag\TagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\PreferenceRequirementInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\restrictionsInfo\restrictionInfo\RestrictionInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\restrictionsInfo\restrictionInfo\restrictionsSt14\RestrictionsSt14TagInterface;
use DOMDocument;
use DOMNode;

class RestrictionInfoTag extends AbstractTag implements RestrictionInfoTagInterface
{
    /**
     * @var RestrictionDto
     */
    protected $data;

    public function setData($data): TagInterface
    {
        if (!$data instanceof RestrictionDto) {
            $this->throwInvalidDataTypeException($data, RestrictionDto::class);
        }

        $this->data = $data;
        return $this;
    }

    public function generate(DOMDocument $domTree): DOMNode
    {
        $restrictionInfo = $domTree->createElement('EPtypes:restrictionInfo');

        $preferenceRequirementInfo = $this->tagProvider->getTag(PreferenceRequirementInfoTagInterface::class);
        $restrictionInfo->appendChild($preferenceRequirementInfo->setData($this->data)->generate($domTree));

        if (!empty($this->data->getNpa())) {
            $restrictionsSt14 = $this->tagProvider->getTag(RestrictionsSt14TagInterface::class);
            $restrictionInfo->appendChild($restrictionsSt14->setData($this->data)->generate($domTree));
        }

        return $restrictionInfo;
    }
}
