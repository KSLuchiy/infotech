<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\restrictionsInfo\restrictionInfo\restrictionsSt14\restrictionSt14;

use common\modules\PurchaseEmulator\dto\request\restriction\RestrictionNpaDto;
use common\modules\Xml\base\tag\AbstractTag;
use common\modules\Xml\base\tag\TagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\restrictionsInfo\restrictionInfo\restrictionsSt14\restrictionSt14\npaInfo\NpaInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\restrictionsInfo\restrictionInfo\restrictionsSt14\restrictionSt14\requirementsType\RequirementsTypeTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\restrictionsInfo\restrictionInfo\restrictionsSt14\restrictionSt14\RestrictionSt14TagInterface;
use DOMDocument;
use DOMNode;

class RestrictionSt14Tag extends AbstractTag implements RestrictionSt14TagInterface
{
    /**
     * @var RestrictionNpaDto
     */
    protected $data;

    public function setData($data): TagInterface
    {
        if (!$data instanceof RestrictionNpaDto) {
            $this->throwInvalidDataTypeException($data, RestrictionNpaDto::class);
        }

        $this->data = $data;
        return $this;
    }

    public function generate(DOMDocument $domTree): DOMNode
    {
        $restrictionSt14 = $domTree->createElement('common:restrictionSt14');

        $requirementsType = $this->tagProvider->getTag(RequirementsTypeTagInterface::class);
        $restrictionSt14->appendChild($requirementsType->setData($this->data)->generate($domTree));

        $npaInfo = $this->tagProvider->getTag(NpaInfoTagInterface::class);
        $restrictionSt14->appendChild($npaInfo->setData($this->data)->generate($domTree));

        return $restrictionSt14;
    }
}
