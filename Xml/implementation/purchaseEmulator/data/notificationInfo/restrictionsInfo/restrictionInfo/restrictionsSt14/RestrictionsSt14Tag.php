<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\restrictionsInfo\restrictionInfo\restrictionsSt14;

use common\modules\PurchaseEmulator\dto\request\restriction\RestrictionDto;
use common\modules\Xml\base\tag\AbstractTag;
use common\modules\Xml\base\tag\TagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\restrictionsInfo\restrictionInfo\restrictionsSt14\RestrictionsSt14TagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\restrictionsInfo\restrictionInfo\restrictionsSt14\restrictionSt14\RestrictionSt14TagInterface;
use DOMDocument;
use DOMNode;

class RestrictionsSt14Tag extends AbstractTag implements RestrictionsSt14TagInterface
{
    /**
     * @var RestrictionDto
     */
    protected $data;

    public function setData($data): TagInterface
    {
        if (!$data instanceof RestrictionDto) {
            $this->throwInvalidDataTypeException($data, RestrictionDto::class);
        }

        $this->data = $data;
        return $this;
    }

    public function generate(DOMDocument $domTree): DOMNode
    {
        $restrictionsSt14 = $domTree->createElement('common:restrictionsSt14');

        foreach ($this->data->getNpa() as $restrictionNpa) {
            $restrictionSt14 = $this->tagProvider->getTag(RestrictionSt14TagInterface::class);
            $restrictionsSt14->appendChild($restrictionSt14->setData($restrictionNpa)->generate($domTree));
        }

        return $restrictionsSt14;
    }
}
