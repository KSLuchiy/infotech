<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\restrictionsInfo\restrictionInfo\restrictionsSt14\restrictionSt14\requirementsType;

use common\modules\PurchaseEmulator\dto\request\restriction\RestrictionNpaDto;
use common\modules\Xml\base\tag\AbstractTag;
use common\modules\Xml\base\tag\TagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\restrictionsInfo\restrictionInfo\restrictionsSt14\restrictionSt14\requirementsType\RequirementsTypeTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\restrictionsInfo\restrictionInfo\restrictionsSt14\restrictionSt14\requirementsType\requirementType\RequirementTypeTagInterface;
use DOMDocument;
use DOMNode;

class RequirementsTypeTag extends AbstractTag implements RequirementsTypeTagInterface
{
    /**
     * @var RestrictionNpaDto
     */
    protected $data;

    public function setData($data): TagInterface
    {
        if (!$data instanceof RestrictionNpaDto) {
            $this->throwInvalidDataTypeException($data, RestrictionNpaDto::class);
        }

        $this->data = $data;
        return $this;
    }

    public function generate(DOMDocument $domTree): DOMNode
    {
        $requirementsType = $domTree->createElement('common:requirementsType');

        $requirementType = $this->tagProvider->getTag(RequirementTypeTagInterface::class);
        $requirementsType->appendChild($requirementType->setData($this->data)->generate($domTree));

        return $requirementsType;
    }
}
