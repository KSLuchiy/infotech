<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\restrictionsInfo\restrictionInfo\restrictionsSt14\restrictionSt14\npaInfo;

use common\components\App;
use common\Exception\NotFoundException;
use common\models\Entity\Nsi\NsiTRUAdmissionNPA;
use common\modules\PurchaseEmulator\dto\request\restriction\RestrictionNpaDto;
use common\modules\Xml\base\provider\TagProviderInterface;
use common\modules\Xml\base\tag\AbstractTag;
use common\modules\Xml\base\tag\TagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\restrictionsInfo\restrictionInfo\restrictionsSt14\restrictionSt14\npaInfo\NpaInfoTagInterface;
use common\Repository\Nsi\NsiTRUAdmissionNPARepository;
use Doctrine\ORM\EntityManager;
use DOMDocument;
use DOMException;
use DOMNode;

class NpaInfoTag extends AbstractTag implements NpaInfoTagInterface
{
    /**
     * @var NsiTRUAdmissionNPARepository
     */
    private $nsiTruAdmissionNPARepository;

    /**
     * @var RestrictionNpaDto
     */
    protected $data;

    public function __construct(TagProviderInterface $tagProvider)
    {
        parent::__construct($tagProvider);

        /* @var EntityManager $entityManagerProcedures */
        $entityManagerProcedures = App::doctrine_pgsql_procedures()->getEntityManager();
        $this->nsiTruAdmissionNPARepository = $entityManagerProcedures->getRepository(NsiTRUAdmissionNPA::class);
    }

    public function setData($data): TagInterface
    {
        if (!$data instanceof RestrictionNpaDto) {
            $this->throwInvalidDataTypeException($data, RestrictionNpaDto::class);
        }

        $this->data = $data;
        return $this;
    }

    /**
     * @throws DOMException
     * @throws NotFoundException
     */
    public function generate(DOMDocument $domTree): DOMNode
    {
        $npa = $this->nsiTruAdmissionNPARepository->findOneBy([
            'code' => $this->data->getNpaNameValue(),
            'actual' => true,
        ]);

        if ($npa === null) {
            throw new NotFoundException('Не найдена опция закупки с code = ' . $this->data->getNpaNameValue());
        }

        $npaInfo = $domTree->createElement('common:NPAInfo');

        $code = $domTree->createElement('base:code', $npa->getCode());
        $npaInfo->appendChild($code);

        $name = $domTree->createElement('base:name', $npa->getName());
        $npaInfo->appendChild($name);

        $shortName = $domTree->createElement('base:shortName', $npa->getShortName());
        $npaInfo->appendChild($shortName);

        return $npaInfo;
    }
}
