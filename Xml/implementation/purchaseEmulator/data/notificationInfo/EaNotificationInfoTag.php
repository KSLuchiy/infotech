<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo;

class EaNotificationInfoTag extends NotificationInfoTag
{
    protected function needFlagsTag(): bool
    {
        return true;
    }
}
