<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\requirementsInfo;

use common\modules\Xml\base\tag\AbstractTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\requirementsInfo\requirementInfo\RequirementInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\requirementsInfo\RequirementsInfoTagInterface;
use DOMDocument;
use DOMNode;

class RequirementsInfoTag extends AbstractTag implements RequirementsInfoTagInterface
{

    public function generate(DOMDocument $domTree): DOMNode
    {
        $requirementsInfo = $domTree->createElement('EPtypes:requirementsInfo');

        foreach ($this->data->getRequirementsInfo() as $requirementDto) {
            $requirementInfoTag = $this->tagProvider->getTag(RequirementInfoTagInterface::class);
            $requirementsInfo->appendChild($requirementInfoTag->setData($requirementDto)->generate($domTree));
        }

        return $requirementsInfo;
    }
}
