<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\requirementsInfo\requirementInfo;

use common\modules\PurchaseEmulator\dto\request\requirement\RequirementDto;
use common\modules\Xml\base\tag\AbstractTag;
use common\modules\Xml\base\tag\TagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\PreferenceRequirementInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\requirementsInfo\requirementInfo\addRequirements\AddRequirementsTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\requirementsInfo\requirementInfo\RequirementInfoTagInterface;
use DOMDocument;
use DOMNode;

class RequirementInfoTag extends AbstractTag implements RequirementInfoTagInterface
{
    /**
     * @var RequirementDto
     */
    protected $data;

    public function setData($data): TagInterface
    {
        if (!$data instanceof RequirementDto) {
            $this->throwInvalidDataTypeException($data, RequirementDto::class);
        }

        $this->data = $data;
        return $this;
    }

    public function generate(DOMDocument $domTree): DOMNode
    {
        $requirementInfo = $domTree->createElement('EPtypes:requirementInfo');

        $preferenceRequirementInfoTag = $this->tagProvider->getTag(PreferenceRequirementInfoTagInterface::class);
        $requirementInfo->appendChild($preferenceRequirementInfoTag->setData($this->data)->generate($domTree));

        if (!empty($this->data->getAdditionalContents())) {
            $addRequirementsTag = $this->tagProvider->getTag(AddRequirementsTagInterface::class);
            $requirementInfo->appendChild($addRequirementsTag->setData($this->data->getAdditionalContents())->generate($domTree));
        }

        return $requirementInfo;
    }
}
