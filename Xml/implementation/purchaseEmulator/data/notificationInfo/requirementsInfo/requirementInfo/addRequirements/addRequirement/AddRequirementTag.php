<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\requirementsInfo\requirementInfo\addRequirements\addRequirement;

use common\components\App;
use common\Exception\InvalidArgumentException;
use common\models\Entity\Postgres\Purchase\Preference\Type;
use common\modules\PurchaseEmulator\dto\request\requirement\RequirementAdditionalDto;
use common\modules\Xml\base\provider\TagProviderInterface;
use common\modules\Xml\base\tag\AbstractTag;
use common\modules\Xml\base\tag\TagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\requirementsInfo\requirementInfo\addRequirements\addRequirement\AddRequirementTagInterface;
use common\Repository\Postgres\Purchase\Preference\TypeRepository;
use Doctrine\ORM\EntityManagerInterface;
use DOMDocument;
use DOMNode;

class AddRequirementTag extends AbstractTag implements AddRequirementTagInterface
{
    /**
     * @var TypeRepository
     */
    private $typeRepository;

    /**
     * @var RequirementAdditionalDto
     */
    protected $data;

    public function __construct(TagProviderInterface $tagProvider)
    {
        parent::__construct($tagProvider);

        /** @var EntityManagerInterface $entityManagerProcedures */
        $entityManagerProcedures = App::doctrine_pgsql_procedures()->getEntityManager();
        $this->typeRepository = $entityManagerProcedures->getRepository(Type::class);
    }

    public function setData($data): TagInterface
    {
        if (!$data instanceof RequirementAdditionalDto) {
            $this->throwInvalidDataTypeException($data, RequirementAdditionalDto::class);
        }

        $this->data = $data;
        return $this;
    }

    public function generate(DOMDocument $domTree): DOMNode
    {
        $addRequirement = $domTree->createElement('common:addRequirement');

        $requirementType = $this->typeRepository->findOneBy([
            'shortName' => $this->data->getAdditionalNameValue(),
            'active' => true,
        ]);

        if ($requirementType === null) {
            throw new InvalidArgumentException('Не найден RequirementType с shortName = ' . $this->data->getAdditionalNameValue());
        }

        $shortName = $domTree->createElement('common:shortName', $requirementType->getShortName());
        $addRequirement->appendChild($shortName);

        $name = $domTree->createElement('common:name', $requirementType->getName());
        $addRequirement->appendChild($name);

        $content = $domTree->createElement('common:content', $this->data->getAdditionalTypeValue());
        $addRequirement->appendChild($content);

        return $addRequirement;
    }
}
