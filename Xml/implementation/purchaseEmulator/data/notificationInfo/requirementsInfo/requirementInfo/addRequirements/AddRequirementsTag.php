<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\requirementsInfo\requirementInfo\addRequirements;

use common\modules\PurchaseEmulator\dto\request\requirement\RequirementAdditionalDto;
use common\modules\Xml\base\tag\AbstractTag;
use common\modules\Xml\base\tag\TagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\requirementsInfo\requirementInfo\addRequirements\addRequirement\AddRequirementTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\requirementsInfo\requirementInfo\addRequirements\AddRequirementsTagInterface;
use DOMDocument;
use DOMNode;

class AddRequirementsTag extends AbstractTag implements AddRequirementsTagInterface
{
    /**
     * @var RequirementAdditionalDto[]
     */
    protected $data;

    public function setData($data): TagInterface
    {
        if (!is_array($data)) {
            $this->throwInvalidDataTypeException($data, 'RequirementAdditionalDto[]');
        }

        $this->data = $data;
        return $this;
    }

    public function generate(DOMDocument $domTree): DOMNode
    {
        $addRequirements = $domTree->createElement('common:addRequirements');
        foreach ($this->data as $additionalContent) {
            $addRequirementTag = $this->tagProvider->getTag(AddRequirementTagInterface::class);
            $addRequirements->appendChild($addRequirementTag->setData($additionalContent)->generate($domTree));
        }

        return $addRequirements;
    }
}
