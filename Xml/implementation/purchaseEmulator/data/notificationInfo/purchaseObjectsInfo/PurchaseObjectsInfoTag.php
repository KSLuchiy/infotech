<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\purchaseObjectsInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractPurchasePublicationDtoTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\purchaseObjectsInfo\notDrugPurchaseObjectsInfo\NotDrugPurchaseObjectsInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\purchaseObjectsInfo\PurchaseObjectsInfoTagInterface;
use DOMDocument;
use DOMNode;

class PurchaseObjectsInfoTag extends AbstractPurchasePublicationDtoTag implements PurchaseObjectsInfoTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $purchaseObjectsInfo = $domTree->createElement('EPtypes:purchaseObjectsInfo');

        $notDrugPurchaseObjectsInfoTag = $this->tagProvider->getTag(NotDrugPurchaseObjectsInfoTagInterface::class);
        $purchaseObjectsInfo->appendChild($notDrugPurchaseObjectsInfoTag->setData($this->data)->generate($domTree));

        return $purchaseObjectsInfo;
    }
}
