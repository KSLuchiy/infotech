<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\purchaseObjectsInfo\notDrugPurchaseObjectsInfo\purchaseObject;

use common\modules\Xml\dataTypeAwareTag\AbstractContractInfoDtoTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\purchaseObjectsInfo\notDrugPurchaseObjectsInfo\purchaseObject\OKEI\OkeiTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\purchaseObjectsInfo\notDrugPurchaseObjectsInfo\purchaseObject\OKPD2\Okpd2TagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\purchaseObjectsInfo\notDrugPurchaseObjectsInfo\purchaseObject\PurchaseObjectTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\purchaseObjectsInfo\notDrugPurchaseObjectsInfo\purchaseObject\quantity\QuantityTagInterface;
use DOMDocument;
use DOMNode;

abstract class AbstractPurchaseObjectTag extends AbstractContractInfoDtoTag implements PurchaseObjectTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $purchaseObjectInfo = $domTree->createElement('common:purchaseObject');

        $sid = $domTree->createElement('common:sid', $this->getSid());
        $purchaseObjectInfo->appendChild($sid);

        $okpd2Tag = $this->tagProvider->getTag(Okpd2TagInterface::class);
        $purchaseObjectInfo->appendChild($okpd2Tag->setData($this->data)->generate($domTree));

        $name = $domTree->createElement('common:name', $this->data->getProductName());
        $purchaseObjectInfo->appendChild($name);

        $okeiTag = $this->tagProvider->getTag(OkeiTagInterface::class);
        $purchaseObjectInfo->appendChild($okeiTag->setData($this->data)->generate($domTree));

        $price = $domTree->createElement('common:price', mb_ereg_replace(' ', '', $this->data->getUnitPrice()));
        $purchaseObjectInfo->appendChild($price);

        $quantityTag = $this->tagProvider->getTag(QuantityTagInterface::class);
        $purchaseObjectInfo->appendChild($quantityTag->setData($this->data)->generate($domTree));

        $sum = $domTree->createElement('common:sum', mb_ereg_replace(' ', '', $this->data->getPrice()));
        $purchaseObjectInfo->appendChild($sum);

        return $purchaseObjectInfo;
    }

    abstract protected function getSid(): int;
}
