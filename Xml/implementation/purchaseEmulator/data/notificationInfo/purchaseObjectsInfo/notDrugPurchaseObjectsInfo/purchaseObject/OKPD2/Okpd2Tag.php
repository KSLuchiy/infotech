<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\purchaseObjectsInfo\notDrugPurchaseObjectsInfo\purchaseObject\OKPD2;

use common\modules\Xml\dataTypeAwareTag\AbstractContractInfoDtoTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\purchaseObjectsInfo\notDrugPurchaseObjectsInfo\purchaseObject\OKPD2\Okpd2TagInterface;
use DOMDocument;
use DOMNode;

class Okpd2Tag extends AbstractContractInfoDtoTag implements Okpd2TagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $okpd2 = $domTree->createElement('common:OKPD2');

        $okpdCode = $domTree->createElement('base:OKPDCode', $this->data->getOkpd2Code());
        $okpd2->appendChild($okpdCode);

        $okpdName = $domTree->createElement('base:OKPDName', $this->data->getOkpd2Name());
        $okpd2->appendChild($okpdName);

        return $okpd2;
    }
}
