<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\purchaseObjectsInfo\notDrugPurchaseObjectsInfo\purchaseObject;

class EaPurchaseObjectTag extends AbstractPurchaseObjectTag
{
    protected function getSid(): int
    {
        return 49349;
    }
}
