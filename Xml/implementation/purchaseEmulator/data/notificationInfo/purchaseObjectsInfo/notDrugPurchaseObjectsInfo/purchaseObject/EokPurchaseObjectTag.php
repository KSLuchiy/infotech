<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\purchaseObjectsInfo\notDrugPurchaseObjectsInfo\purchaseObject;

class EokPurchaseObjectTag extends AbstractPurchaseObjectTag
{
    protected function getSid(): int
    {
        return 92638;
    }
}
