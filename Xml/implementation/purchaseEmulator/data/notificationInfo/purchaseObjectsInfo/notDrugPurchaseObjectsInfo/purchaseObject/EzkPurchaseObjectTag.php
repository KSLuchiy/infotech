<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\purchaseObjectsInfo\notDrugPurchaseObjectsInfo\purchaseObject;

class EzkPurchaseObjectTag extends AbstractPurchaseObjectTag
{
    protected function getSid(): int
    {
        return 50110;
    }
}
