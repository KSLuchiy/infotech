<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\purchaseObjectsInfo\notDrugPurchaseObjectsInfo\purchaseObject;

class EokPsdPurchaseObjectTag extends AbstractPurchaseObjectTag
{
    protected function getSid(): int
    {
        return 92658;
    }
}
