<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\purchaseObjectsInfo\notDrugPurchaseObjectsInfo\purchaseObject\quantity;

use common\modules\Xml\dataTypeAwareTag\AbstractContractInfoDtoTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\purchaseObjectsInfo\notDrugPurchaseObjectsInfo\purchaseObject\quantity\QuantityTagInterface;
use DOMDocument;
use DOMNode;

class QuantityTag extends AbstractContractInfoDtoTag implements QuantityTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $quantity = $domTree->createElement('common:quantity');

        $value = $domTree->createElement('common:value', mb_ereg_replace(' ', '', $this->data->getProductQuantity()));
        $quantity->appendChild($value);

        return $quantity;
    }
}
