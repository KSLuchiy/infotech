<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\purchaseObjectsInfo\notDrugPurchaseObjectsInfo\purchaseObject\OKEI;

use common\modules\Xml\dataTypeAwareTag\AbstractContractInfoDtoTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\purchaseObjectsInfo\notDrugPurchaseObjectsInfo\purchaseObject\OKEI\OkeiTagInterface;
use DOMDocument;
use DOMNode;

class OkeiTag extends AbstractContractInfoDtoTag implements OkeiTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $okei = $domTree->createElement('common:OKEI');

        $code = $domTree->createElement('base:code', $this->data->getMeasureUnitCode());
        $okei->appendChild($code);

        $nationalCode = $domTree->createElement('base:nationalCode', $this->data->getMeasureUnitNationalCode());
        $okei->appendChild($nationalCode);

        $name = $domTree->createElement('base:name', $this->data->getMeasureUnitName());
        $okei->appendChild($name);

        return $okei;
    }
}
