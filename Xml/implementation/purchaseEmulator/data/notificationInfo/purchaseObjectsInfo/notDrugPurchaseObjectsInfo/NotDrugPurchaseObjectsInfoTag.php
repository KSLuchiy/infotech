<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\purchaseObjectsInfo\notDrugPurchaseObjectsInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractPurchasePublicationDtoTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\purchaseObjectsInfo\notDrugPurchaseObjectsInfo\NotDrugPurchaseObjectsInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\purchaseObjectsInfo\notDrugPurchaseObjectsInfo\purchaseObject\PurchaseObjectTagInterface;
use DOMDocument;
use DOMNode;

class NotDrugPurchaseObjectsInfoTag extends AbstractPurchasePublicationDtoTag implements NotDrugPurchaseObjectsInfoTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $notDrugPurchaseObjectsInfo = $domTree->createElement('EPtypes:notDrugPurchaseObjectsInfo');

        foreach ($this->data->getContractsInfo()->getContractInfo() as $contractInfo) {
            $purchaseObjectTag = $this->tagProvider->getTag(PurchaseObjectTagInterface::class);
            $notDrugPurchaseObjectsInfo->appendChild($purchaseObjectTag->setData($contractInfo)->generate($domTree));
        }

        $totalSum = $domTree->createElement('common:totalSum', mb_ereg_replace(' ', '', $this->data->getContractsInfo()->getTotalPrice()));
        $notDrugPurchaseObjectsInfo->appendChild($totalSum);

        $quantityUndefined = $domTree->createElement('EPtypes:quantityUndefined', 'false');
        $notDrugPurchaseObjectsInfo->appendChild($quantityUndefined);

        return $notDrugPurchaseObjectsInfo;
    }
}
