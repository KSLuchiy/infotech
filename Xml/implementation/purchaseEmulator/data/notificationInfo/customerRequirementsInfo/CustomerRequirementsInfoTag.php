<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\customerRequirementsInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractPurchasePublicationDtoTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\CustomerRequirementInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\CustomerRequirementsInfoTagInterface;
use DOMDocument;
use DOMNode;

class CustomerRequirementsInfoTag extends AbstractPurchasePublicationDtoTag implements CustomerRequirementsInfoTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $customerRequirementsInfo = $domTree->createElement('EPtypes:customerRequirementsInfo');

        foreach ($this->data->getCustomersInfo() as $customer) {
            $customerRequirementInfo = $this->tagProvider->getTag(CustomerRequirementInfoTagInterface::class);
            $customerRequirementsInfo->appendChild($customerRequirementInfo->setData($customer)->generate($domTree));
        }

        return $customerRequirementsInfo;
    }
}
