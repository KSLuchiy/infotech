<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\applicationGuarantee;

class EokApplicationGuaranteeTag extends ApplicationGuaranteeTag
{
    protected function getPathToXml(): string
    {
        return 'purchaseEmulator/eok/applicationGuarantee.xml';
    }
}
