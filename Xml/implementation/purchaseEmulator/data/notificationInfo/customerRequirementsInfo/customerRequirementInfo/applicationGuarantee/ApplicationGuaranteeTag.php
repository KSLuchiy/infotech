<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\applicationGuarantee;

use common\modules\PurchaseEmulator\dto\request\CustomerInfoDto;
use common\modules\Xml\base\tag\AbstractFromXmlTag;
use common\modules\Xml\base\tag\TagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\applicationGuarantee\ApplicationGuaranteeTagInterface;
use DOMDocument;
use DOMNode;

class ApplicationGuaranteeTag extends AbstractFromXmlTag implements ApplicationGuaranteeTagInterface
{
    /**
     * @var CustomerInfoDto
     */
    protected $data;

    public function setData($data): TagInterface
    {
        if (!$data instanceof CustomerInfoDto) {
            $this->throwInvalidDataTypeException($data, CustomerInfoDto::class);
        }

        $this->data = $data;
        return $this;
    }

    public function generate(DOMDocument $domTree): DOMNode
    {
        $applicationGuarantee = parent::generate($domTree);

        $amount = $domTree->createElement('EPtypes:amount', mb_ereg_replace(' ', '', $this->data->getApplicationAmount()));
        $applicationGuarantee->insertBefore($amount, $applicationGuarantee->childNodes[0]);

        return $applicationGuarantee;
    }

    protected function getPathToXml(): string
    {
        return 'purchaseEmulator/common/applicationGuarantee.xml';
    }
}
