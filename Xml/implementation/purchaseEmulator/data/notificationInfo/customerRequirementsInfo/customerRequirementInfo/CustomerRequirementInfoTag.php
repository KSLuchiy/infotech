<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractCustomerInfoDtoTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\applicationGuarantee\ApplicationGuaranteeTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractConditionsInfo\ContractConditionsInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractGuarantee\ContractGuaranteeTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\customer\CustomerTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\CustomerRequirementInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\provisionWarranty\ProvisionWarrantyTagInterface;
use DOMDocument;
use DOMNode;

class CustomerRequirementInfoTag extends AbstractCustomerInfoDtoTag implements CustomerRequirementInfoTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $customerRequirementInfo = $domTree->createElement('EPtypes:customerRequirementInfo');

        $customer = $this->tagProvider->getTag(CustomerTagInterface::class);
        $customerRequirementInfo->appendChild($customer->setData($this->data)->generate($domTree));

        $applicationGuarantee = $this->tagProvider->getTag(ApplicationGuaranteeTagInterface::class);
        $customerRequirementInfo->appendChild($applicationGuarantee->setData($this->data)->generate($domTree));

        $contractGuarantee = $this->tagProvider->getTag(ContractGuaranteeTagInterface::class);
        $customerRequirementInfo->appendChild($contractGuarantee->setData($this->data)->generate($domTree));

        $contractConditionsInfo = $this->tagProvider->getTag(ContractConditionsInfoTagInterface::class);
        $customerRequirementInfo->appendChild($contractConditionsInfo->setData($this->data)->generate($domTree));

        $provisionWarranty = $this->tagProvider->getTag(ProvisionWarrantyTagInterface::class);
        $customerRequirementInfo->appendChild($provisionWarranty->setData($this->data)->generate($domTree));

        return $customerRequirementInfo;
    }
}
