<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractConditionsInfo\ikzInfo;

use common\modules\PurchaseEmulator\dto\request\CustomerInfoDto;
use common\modules\Xml\base\tag\AbstractFromXmlTag;
use common\modules\Xml\base\tag\TagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractConditionsInfo\ikzInfo\IkzInfoTagInterface;
use DOMDocument;
use DOMNode;

class IkzInfoTag extends AbstractFromXmlTag implements IkzInfoTagInterface
{
    /**
     * @var CustomerInfoDto
     */
    protected $data;

    public function setData($data): TagInterface
    {
        if (!$data instanceof CustomerInfoDto) {
            $this->throwInvalidDataTypeException($data, CustomerInfoDto::class);
        }

        $this->data = $data;
        return $this;
    }

    public function generate(DOMDocument $domTree): DOMNode
    {
        $ikzInfo = parent::generate($domTree);

        $purchaseCode = $domTree->createElement('EPtypes:purchaseCode', $this->data->getIdCode());
        $ikzInfo->insertBefore($purchaseCode, $ikzInfo->childNodes[0]);

        return $ikzInfo;
    }

    protected function getPathToXml(): string
    {
        return 'purchaseEmulator/common/ikzInfo.xml';
    }
}
