<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractConditionsInfo\deliveryPlacesInfo;

use common\modules\Xml\base\tag\AbstractFromXmlTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractConditionsInfo\deliveryPlacesInfo\DeliveryPlacesInfoTagInterface;


class EokDeliveryPlacesInfoTag extends AbstractFromXmlTag implements DeliveryPlacesInfoTagInterface
{
    protected function getPathToXml(): string
    {
        return 'purchaseEmulator/eok/deliveryPlacesInfo.xml';
    }
}
