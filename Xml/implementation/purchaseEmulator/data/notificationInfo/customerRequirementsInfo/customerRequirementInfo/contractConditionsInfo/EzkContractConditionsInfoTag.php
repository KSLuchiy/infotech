<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractConditionsInfo;

class EzkContractConditionsInfoTag extends ContractConditionsInfoTag
{
    protected function needMustPublicDiscussionTag(): bool
    {
        return false;
    }

    protected function needOneSideRejectionSt95Tag(): bool
    {
        return false;
    }
}
