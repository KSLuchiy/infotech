<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractConditionsInfo\BOInfo;


use common\modules\Xml\base\tag\AbstractFromXmlTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractConditionsInfo\BOInfo\BOInfoTagInterface;

class BOInfoTag extends AbstractFromXmlTag implements BOInfoTagInterface
{
    protected function getPathToXml(): string
    {
        return 'purchaseEmulator/common/BOInfo.xml';
    }
}
