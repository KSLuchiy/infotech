<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractConditionsInfo\contractExecutionPaymentPlan;

use common\modules\Xml\base\tag\AbstractFromXmlTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractConditionsInfo\contractExecutionPaymentPlan\ContractExecutionPaymentPlanTagInterface;


class ContractExecutionPaymentPlanTag extends AbstractFromXmlTag implements ContractExecutionPaymentPlanTagInterface
{
    protected function getPathToXml(): string
    {
        return 'purchaseEmulator/common/contractExecutionPaymentPlan.xml';
    }
}
