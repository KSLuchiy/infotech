<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractConditionsInfo\maxPriceInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractCustomerInfoDtoTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractConditionsInfo\maxPriceInfo\MaxPriceInfoTagInterface;
use DOMDocument;
use DOMNode;

class MaxPriceInfoTag extends AbstractCustomerInfoDtoTag implements MaxPriceInfoTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $maxPriceInfo = $domTree->createElement('EPtypes:maxPriceInfo');

        $maxPrice = $domTree->createElement('EPtypes:maxPrice', mb_ereg_replace(' ', '', $this->data->getInitialPrice()));
        $maxPriceInfo->appendChild($maxPrice);

        return $maxPriceInfo;
    }
}
