<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractConditionsInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractCustomerInfoDtoTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractConditionsInfo\BOInfo\BOInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractConditionsInfo\ContractConditionsInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractConditionsInfo\contractExecutionPaymentPlan\ContractExecutionPaymentPlanTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractConditionsInfo\deliveryPlacesInfo\DeliveryPlacesInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractConditionsInfo\ikzInfo\IkzInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractConditionsInfo\maxPriceInfo\MaxPriceInfoTagInterface;
use DOMDocument;
use DOMNode;

class ContractConditionsInfoTag extends AbstractCustomerInfoDtoTag implements ContractConditionsInfoTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $contractConditionsInfo = $domTree->createElement('EPtypes:contractConditionsInfo');

        $maxPriceInfo = $this->tagProvider->getTag(MaxPriceInfoTagInterface::class);
        $contractConditionsInfo->appendChild($maxPriceInfo->setData($this->data)->generate($domTree));

        if ($this->needMustPublicDiscussionTag()) {
            $mustPublicDiscussion = $domTree->createElement('EPtypes:mustPublicDiscussion', 'false');
            $contractConditionsInfo->appendChild($mustPublicDiscussion);
        }

        $IKZInfo = $this->tagProvider->getTag(IkzInfoTagInterface::class);
        $contractConditionsInfo->appendChild($IKZInfo->setData($this->data)->generate($domTree));

        $contractExecutionPaymentPlan = $this->tagProvider->getTag(ContractExecutionPaymentPlanTagInterface::class);
        $contractConditionsInfo->appendChild($contractExecutionPaymentPlan->generate($domTree));

        $BOInfo = $this->tagProvider->getTag(BOInfoTagInterface::class);
        $contractConditionsInfo->appendChild($BOInfo->generate($domTree));

        $deliveryPlacesInfo = $this->tagProvider->getTag(DeliveryPlacesInfoTagInterface::class);
        $contractConditionsInfo->appendChild($deliveryPlacesInfo->generate($domTree));

        $deliveryTerm = $domTree->createElement('EPtypes:deliveryTerm', 'Срок исполнения контракта, отдельных этапов исполнения контракта*');
        $contractConditionsInfo->appendChild($deliveryTerm);

        if ($this->needOneSideRejectionSt95Tag()) {
            $oneSideRejectionSt95 = $domTree->createElement('EPtypes:oneSideRejectionSt95', 'Какой-то текст');
            $contractConditionsInfo->appendChild($oneSideRejectionSt95);
        }

        return $contractConditionsInfo;
    }

    protected function needMustPublicDiscussionTag(): bool
    {
        return true;
    }

    protected function needOneSideRejectionSt95Tag(): bool
    {
        return true;
    }
}
