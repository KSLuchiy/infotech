<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\customer;

use common\components\App;
use common\Exception\NotFoundException;
use common\models\Entity\Postgres\Organisation\Organisation;
use common\models\Entity\Postgres\RefRecordStatus;
use common\modules\Xml\base\provider\TagProviderInterface;
use common\modules\Xml\dataTypeAwareTag\AbstractCustomerInfoDtoTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\customer\CustomerTagInterface;
use common\Repository\Postgres\Organisation\OrganisationRepository;
use Doctrine\ORM\EntityManagerInterface;
use DOMDocument;
use DOMException;
use DOMNode;

class CustomerTag extends AbstractCustomerInfoDtoTag implements CustomerTagInterface
{
    /**
     * @var OrganisationRepository
     */
    protected $organisationRepository;

    public function __construct(TagProviderInterface $tagProvider)
    {
        parent::__construct($tagProvider);

        /** @var EntityManagerInterface $entityManagerEtp */
        $entityManagerEtp = App::doctrine_pgsql()->getEntityManager();
        $this->organisationRepository = $entityManagerEtp->getRepository(Organisation::class);
    }

    /**
     * @throws DOMException
     * @throws NotFoundException
     */
    public function generate(DOMDocument $domTree): DOMNode
    {
        $organisation = $this->organisationRepository->findOneBy([
            'reg_number' => $this->data->getRegNumber(),
            'record_status_id' => RefRecordStatus::ACTIVE,
        ]);

        if ($organisation === null) {
            throw new NotFoundException('Не найдена организация с reg_number = ' . $this->data->getRegNumber());
        }

        $customer = $domTree->createElement('EPtypes:customer');

        $regNum = $domTree->createElement('base:regNum', $organisation->getRegNumber());
        $customer->appendChild($regNum);

        $consRegistryNum = $domTree->createElement('base:consRegistryNum', $organisation->getConsRegistryNum());
        $customer->appendChild($consRegistryNum);

        $fullName = $domTree->createElement('base:fullName', $organisation->getFullName());
        $customer->appendChild($fullName);

        return $customer;
    }
}
