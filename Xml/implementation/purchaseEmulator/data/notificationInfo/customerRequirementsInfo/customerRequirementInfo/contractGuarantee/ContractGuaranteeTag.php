<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractGuarantee;

use common\modules\PurchaseEmulator\dto\request\CustomerInfoDto;
use common\modules\Xml\base\tag\AbstractFromXmlTag;
use common\modules\Xml\base\tag\TagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractGuarantee\ContractGuaranteeTagInterface;
use DOMDocument;
use DOMNode;


class ContractGuaranteeTag extends AbstractFromXmlTag implements ContractGuaranteeTagInterface
{
    /**
     * @var CustomerInfoDto
     */
    protected $data;

    public function setData($data): TagInterface
    {
        if (!$data instanceof CustomerInfoDto) {
            $this->throwInvalidDataTypeException($data, CustomerInfoDto::class);
        }

        $this->data = $data;
        return $this;
    }

    public function generate(DOMDocument $domTree): DOMNode
    {
        $contractGuarantee = parent::generate($domTree);

        $amount = $domTree->createElement('EPtypes:amount', mb_ereg_replace(' ', '', $this->data->getContractAmount()));
        $contractGuarantee->insertBefore($amount, $contractGuarantee->childNodes[0]);

        return $contractGuarantee;
    }

    protected function getPathToXml(): string
    {
        return 'purchaseEmulator/common/contractGuarantee.xml';
    }
}
