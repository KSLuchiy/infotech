<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\provisionWarranty;

class EokProvisionWarrantyTag extends AbstractProvisionWarrantyTag
{
    protected function getPathToXml(): string
    {
        return 'purchaseEmulator/eok/provisionWarranty.xml';
    }
}
