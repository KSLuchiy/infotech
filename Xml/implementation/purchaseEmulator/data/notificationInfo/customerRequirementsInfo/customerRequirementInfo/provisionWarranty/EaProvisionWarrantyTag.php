<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\provisionWarranty;

class EaProvisionWarrantyTag extends AbstractProvisionWarrantyTag
{
    protected function getPathToXml(): string
    {
        return 'purchaseEmulator/ea/provisionWarranty.xml';
    }
}
