<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\provisionWarranty;

class EzkProvisionWarrantyTag extends AbstractProvisionWarrantyTag
{
    protected function getPathToXml(): string
    {
        return 'purchaseEmulator/ezk/provisionWarranty.xml';
    }
}
