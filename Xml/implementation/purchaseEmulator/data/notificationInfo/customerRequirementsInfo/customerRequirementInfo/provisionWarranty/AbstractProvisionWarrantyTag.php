<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\provisionWarranty;

use common\modules\Xml\base\tag\AbstractFromXmlTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\provisionWarranty\ProvisionWarrantyTagInterface;
use DOMDocument;
use DOMNode;


abstract class AbstractProvisionWarrantyTag extends AbstractFromXmlTag implements ProvisionWarrantyTagInterface
{

    public function generate(DOMDocument $domTree): DOMNode
    {
        $provisionWarranty = parent::generate($domTree);

        $amount = $domTree->createElement('common:amount', mb_ereg_replace(' ', '', $this->data->getWarrantyAmount()));
        $provisionWarranty->insertBefore($amount, $provisionWarranty->childNodes[0]);

        return $provisionWarranty;
    }
}
