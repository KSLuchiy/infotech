<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo;

class EokNotificationInfoTag extends NotificationInfoTag
{
    protected function needFlagsTag(): bool
    {
        return true;
    }

    protected function needCriteriaInfoTag(): bool
    {
        return true;
    }
}
