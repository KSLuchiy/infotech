<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\preferencesInfo\preferenceInfo;

use common\modules\PurchaseEmulator\dto\request\preference\PreferenceDto;
use common\modules\Xml\base\tag\AbstractTag;
use common\modules\Xml\base\tag\TagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\PreferenceRequirementInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\preferencesInfo\preferenceInfo\PreferenceInfoTagInterface;
use DOMDocument;
use DOMNode;

class PreferenceInfoTag extends AbstractTag implements PreferenceInfoTagInterface
{
    /**
     * @var PreferenceDto
     */
    protected $data;

    public function setData($data): TagInterface
    {
        if (!$data instanceof PreferenceDto) {
            $this->throwInvalidDataTypeException($data, PreferenceDto::class);
        }

        $this->data = $data;
        return $this;
    }

    public function generate(DOMDocument $domTree): DOMNode
    {
        $preferenceInfo = $domTree->createElement('EPtypes:preferenseInfo');

        $preferenceRequirementInfo = $this->tagProvider->getTag(PreferenceRequirementInfoTagInterface::class);
        $preferenceInfo->appendChild($preferenceRequirementInfo->setData($this->data)->generate($domTree));

        if (!empty($this->data->getQuantity())) {
            $prefValue = $domTree->createElement('common:prefValue', $this->data->getQuantity());
            $preferenceInfo->appendChild($prefValue);
        }

        return $preferenceInfo;
    }
}
