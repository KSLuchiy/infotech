<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\preferencesInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractPurchasePublicationDtoTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\preferencesInfo\preferenceInfo\PreferenceInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\preferencesInfo\PreferencesInfoTagInterface;
use DOMDocument;
use DOMNode;

class PreferencesInfoTag extends AbstractPurchasePublicationDtoTag implements PreferencesInfoTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $preferencesInfo = $domTree->createElement('EPtypes:preferensesInfo');

        foreach ($this->data->getPreferencesInfo() as $preference) {
            $preferenceInfo = $this->tagProvider->getTag(PreferenceInfoTagInterface::class);
            $preferencesInfo->appendChild($preferenceInfo->setData($preference)->generate($domTree));
        }

        return $preferencesInfo;
    }
}
