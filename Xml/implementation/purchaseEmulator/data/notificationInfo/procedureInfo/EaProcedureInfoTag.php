<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\procedureInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractPurchasePublicationDtoTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\procedureInfo\collectingInfo\CollectingInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\procedureInfo\ProcedureInfoTagInterface;
use common\modules\Xml\traits\DateTrait;
use DOMDocument;
use DOMNode;

class EaProcedureInfoTag extends AbstractPurchasePublicationDtoTag implements ProcedureInfoTagInterface
{
    use DateTrait;

    private const DATE_FORMAT = 'Y-m-d';

    public function generate(DOMDocument $domTree): DOMNode
    {
        $procedureInfo = $domTree->createElement('EPtypes:procedureInfo');

        $collectingInfoTag = $this->tagProvider->getTag(CollectingInfoTagInterface::class);
        $procedureInfo->appendChild($collectingInfoTag->setData($this->data)->generate($domTree));

        $biddingDateValue = $this->datePlusIntervalFormatted($this->data->getPublicationDateTime(), '3 day 2 hour', self::DATE_FORMAT);
        $biddingDate = $domTree->createElement('EPtypes:biddingDate', $biddingDateValue);
        $procedureInfo->appendChild($biddingDate);

        $summarizingDate = $domTree->createElement('EPtypes:summarizingDate', $this->data->getResultDateTime());
        $procedureInfo->appendChild($summarizingDate);

        return $procedureInfo;
    }
}
