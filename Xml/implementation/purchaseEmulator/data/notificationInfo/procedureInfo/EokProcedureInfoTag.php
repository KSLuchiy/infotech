<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\procedureInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractPurchasePublicationDtoTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\procedureInfo\collectingInfo\CollectingInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\procedureInfo\ProcedureInfoTagInterface;
use common\modules\Xml\traits\DateTrait;
use DOMDocument;
use DOMNode;

class EokProcedureInfoTag extends AbstractPurchasePublicationDtoTag implements ProcedureInfoTagInterface
{
    use DateTrait;

    private const DATE_TIME_FORMAT = 'Y-m-d\TH:i:sP';

    public function generate(DOMDocument $domTree): DOMNode
    {
        $procedureInfo = $domTree->createElement('EPtypes:procedureInfo');

        $collectingInfoTag = $this->tagProvider->getTag(CollectingInfoTagInterface::class);
        $procedureInfo->appendChild($collectingInfoTag->setData($this->data)->generate($domTree));

        $firstPartsDateValue = $this->data->getFirstPartsDateTime()->format(self::DATE_TIME_FORMAT);
        $firstPartsDate = $domTree->createElement('EPtypes:firstPartsDate', $firstPartsDateValue);
        $procedureInfo->appendChild($firstPartsDate);

        $submissionProcedureDateValue = $this->data->getConditionalHoldingDateTime();
        $submissionProcedureDate = $domTree->createElement('EPtypes:submissionProcedureDate', $submissionProcedureDateValue);
        $procedureInfo->appendChild($submissionProcedureDate);

        $secondPartsDateValue = $this->data->getSecondPartsDateTime()->format(self::DATE_TIME_FORMAT);
        $secondPartsDate = $domTree->createElement('EPtypes:secondPartsDate', $secondPartsDateValue);
        $procedureInfo->appendChild($secondPartsDate);

        $summarizingDateValue = $this->data->getResultDateTime();
        $summarizingDate = $domTree->createElement('EPtypes:summarizingDate', $summarizingDateValue);
        $procedureInfo->appendChild($summarizingDate);

        return $procedureInfo;
    }
}
