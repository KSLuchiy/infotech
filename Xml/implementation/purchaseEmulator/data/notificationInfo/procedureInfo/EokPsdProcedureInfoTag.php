<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\procedureInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractPurchasePublicationDtoTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\procedureInfo\collectingInfo\CollectingInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\procedureInfo\ProcedureInfoTagInterface;
use DOMDocument;
use DOMNode;

class EokPsdProcedureInfoTag extends AbstractPurchasePublicationDtoTag implements ProcedureInfoTagInterface
{
    private const DATE_FORMAT = 'Y-m-d\TH:i:sP';

    public function generate(DOMDocument $domTree): DOMNode
    {
        $procedureInfo = $domTree->createElement('EPtypes:procedureInfo');

        $collectingInfoTag = $this->tagProvider->getTag(CollectingInfoTagInterface::class);
        $procedureInfo->appendChild($collectingInfoTag->setData($this->data)->generate($domTree));

        $secondPartsDateValue = $this->data->getSecondPartsDateTime()->format(self::DATE_FORMAT);
        $secondPartsDate = $domTree->createElement('EPtypes:secondPartsDate', $secondPartsDateValue);
        $procedureInfo->appendChild($secondPartsDate);

        $summarizingDateValue = $this->data->getResultDateTime();
        $summarizingDate = $domTree->createElement('EPtypes:summarizingDate', $summarizingDateValue);
        $procedureInfo->appendChild($summarizingDate);

        return $procedureInfo;
    }
}
