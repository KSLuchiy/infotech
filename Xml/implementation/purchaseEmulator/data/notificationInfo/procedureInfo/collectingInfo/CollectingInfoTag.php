<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\procedureInfo\collectingInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractPurchasePublicationDtoTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\procedureInfo\collectingInfo\CollectingInfoTagInterface;
use DOMDocument;
use DOMNode;

class CollectingInfoTag extends AbstractPurchasePublicationDtoTag implements CollectingInfoTagInterface
{
    protected const DATE_FORMAT = 'Y-m-d\TH:i:sP';

    public function generate(DOMDocument $domTree): DOMNode
    {
        $collectingInfo = $domTree->createElement('EPtypes:collectingInfo');

        $startDTValue = $this->data->getPublicationDateTime()->format(self::DATE_FORMAT);
        $startDT = $domTree->createElement('EPtypes:startDT', $startDTValue);
        $collectingInfo->appendChild($startDT);

        $endDT = $domTree->createElement('EPtypes:endDT', $this->data->getRequestsClosingDateTime()->format(self::DATE_FORMAT));
        $collectingInfo->appendChild($endDT);

        return $collectingInfo;
    }
}
