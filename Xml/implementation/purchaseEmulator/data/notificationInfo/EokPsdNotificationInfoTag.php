<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo;

class EokPsdNotificationInfoTag extends NotificationInfoTag
{
    protected function needCriteriaInfoTag(): bool
    {
        return true;
    }

    protected function needFlagsTag(): bool
    {
        return true;
    }
}
