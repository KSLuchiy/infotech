<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo;

use common\components\App;
use common\Exception\NotFoundException;
use common\models\Entity\Postgres\Purchase\Preference\Type as PreferenceType;
use common\modules\PurchaseEmulator\dto\PreferenceRequirementValueInterface;
use common\modules\Xml\base\provider\TagProviderInterface;
use common\modules\Xml\base\tag\AbstractTag;
use common\modules\Xml\base\tag\TagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\PreferenceRequirementInfoTagInterface;
use common\Repository\Postgres\Purchase\Preference\TypeRepository as PreferenceTypeRepository;
use Doctrine\ORM\EntityManager;
use DOMDocument;
use DOMException;
use DOMNode;

class PreferenceRequirementInfoTag extends AbstractTag implements PreferenceRequirementInfoTagInterface
{
    /**
     * @var PreferenceTypeRepository
     */
    private $preferenceTypeRepository;

    /**
     * @var PreferenceRequirementValueInterface
     */
    private $preferenceRequirementValue;

    public function __construct(TagProviderInterface $tagProvider)
    {
        parent::__construct($tagProvider);

        /* @var EntityManager $entityManagerProcedures */
        $entityManagerProcedures = App::doctrine_pgsql_procedures()->getEntityManager();
        $this->preferenceTypeRepository = $entityManagerProcedures->getRepository(PreferenceType::class);
    }

    public function setData($data): TagInterface
    {
        if (!$data instanceof PreferenceRequirementValueInterface) {
            $this->throwInvalidDataTypeException($data, PreferenceRequirementValueInterface::class);
        }

        $this->preferenceRequirementValue = $data;
        return $this;
    }

    /**
     * @throws DOMException
     * @throws NotFoundException
     */
    public function generate(DOMDocument $domTree): DOMNode
    {
        $preference = $this->preferenceTypeRepository->findOneBy([
            'shortName' => $this->preferenceRequirementValue->getValue(),
            'active' => true,
        ]);

        if ($preference === null) {
            throw new NotFoundException('Не найдена опция закупки с shortName = ' . $this->preferenceRequirementValue->getValue());
        }

        $preferenseRequirementInfo = $domTree->createElement('common:preferenseRequirementInfo');

        $shortName = $domTree->createElement('base:shortName', $preference->getShortName());
        $preferenseRequirementInfo->appendChild($shortName);

        $name = $domTree->createElement('base:name', $preference->getName());
        $preferenseRequirementInfo->appendChild($name);

        return $preferenseRequirementInfo;
    }
}
