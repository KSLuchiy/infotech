<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo\qualitativeCriterionInfo\criterionInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractCriteriaDtoClass;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo\qualitativeCriterionInfo\criterionInfo\CriterionInfoTagInterface;
use DOMDocument;
use DOMNode;

class CriterionInfoTag extends AbstractCriteriaDtoClass implements CriterionInfoTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $criterionInfo = $domTree->createElement('EPtypes:criterionInfo');

        if ($this->data->getLimitValue() !== null) {
            $limit = $domTree->createElement('EPtypes:limit', $this->data->getLimitValue());
            $criterionInfo->appendChild($limit);
        }

        $measurementOrder = $domTree->createElement('EPtypes:measurementOrder', $this->data->getGradeOrder());
        $criterionInfo->appendChild($measurementOrder);

        return $criterionInfo;
    }
}
