<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo\qualitativeCriterionInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractCriteriaDtoClass;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo\qualitativeCriterionInfo\criterionInfo\CriterionInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo\qualitativeCriterionInfo\indicatorsInfo\IndicatorsInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo\qualitativeCriterionInfo\QualitativeCriterionInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo\valueInfo\ValueInfoTagInterface;
use DOMDocument;
use DOMNode;

class QualitativeCriterionInfoTag extends AbstractCriteriaDtoClass implements QualitativeCriterionInfoTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $qualitativeCriterionInfo = $domTree->createElement('EPtypes:qualitativeCriterionInfo');

        $code = $domTree->createElement('EPtypes:code', $this->data->getCode());
        $qualitativeCriterionInfo->appendChild($code);

        $valueInfo = $this->tagProvider->getTag(ValueInfoTagInterface::class);
        $qualitativeCriterionInfo->appendChild($valueInfo->setData($this->data)->generate($domTree));

        if ($this->data->getIndicatorCriterionValue()) {
            $indicatorsInfo = $this->tagProvider->getTag(IndicatorsInfoTagInterface::class);
            $qualitativeCriterionInfo->appendChild($indicatorsInfo->setData($this->data)->generate($domTree));
        } else {
            $criterionInfo = $this->tagProvider->getTag(CriterionInfoTagInterface::class);
            $qualitativeCriterionInfo->appendChild($criterionInfo->setData($this->data)->generate($domTree));
        }

        return $qualitativeCriterionInfo;
    }
}
