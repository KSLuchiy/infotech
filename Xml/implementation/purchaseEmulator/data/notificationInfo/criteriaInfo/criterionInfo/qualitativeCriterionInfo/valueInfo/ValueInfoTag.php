<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo\qualitativeCriterionInfo\valueInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractCriteriaDtoClass;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo\valueInfo\ValueInfoTagInterface;
use DOMDocument;
use DOMNode;

class ValueInfoTag extends AbstractCriteriaDtoClass implements ValueInfoTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $valueInfo = $domTree->createElement('EPtypes:valueInfo');

        $value = $domTree->createElement('EPtypes:value', $this->data->getSignificanceCriterion());
        $valueInfo->appendChild($value);

        return $valueInfo;
    }
}
