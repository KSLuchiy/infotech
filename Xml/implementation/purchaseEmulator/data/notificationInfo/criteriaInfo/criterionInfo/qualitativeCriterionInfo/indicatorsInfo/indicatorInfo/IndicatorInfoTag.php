<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo\qualitativeCriterionInfo\indicatorsInfo\indicatorInfo;

use common\modules\PurchaseEmulator\dto\request\criteria\CriteriaIndicatorDto;
use common\modules\Xml\base\tag\AbstractTag;
use common\modules\Xml\base\tag\TagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo\qualitativeCriterionInfo\indicatorsInfo\indicatorInfo\IndicatorInfoTagInterface;
use DOMDocument;
use DOMNode;

class IndicatorInfoTag extends AbstractTag implements IndicatorInfoTagInterface
{
    /**
     * @var CriteriaIndicatorDto
     */
    protected $data;

    public function setData($data): TagInterface
    {
        if (!$data instanceof CriteriaIndicatorDto) {
            $this->throwInvalidDataTypeException($data, CriteriaIndicatorDto::class);
        }

        $this->data = $data;
        return $this;
    }

    public function generate(DOMDocument $domTree): DOMNode
    {
        $indicatorInfo = $domTree->createElement('EPtypes:indicatorInfo');

        $sId = $domTree->createElement('EPtypes:sId', '74058');
        $indicatorInfo->appendChild($sId);

        $name = $domTree->createElement('EPtypes:name', $this->data->getIndicatorName());
        $indicatorInfo->appendChild($name);

        $value = $domTree->createElement('EPtypes:value', $this->data->getSignificanceIndicator());
        $indicatorInfo->appendChild($value);

        if ($this->data->getMaxValueIndicator() !== null) {
            $limit = $domTree->createElement('EPtypes:limit', $this->data->getMaxValueIndicator());
            $indicatorInfo->appendChild($limit);
        }

        $measurementOrder = $domTree->createElement('EPtypes:measurementOrder', $this->data->getIndicatorOrderGrade());
        $indicatorInfo->appendChild($measurementOrder);

        return $indicatorInfo;
    }
}
