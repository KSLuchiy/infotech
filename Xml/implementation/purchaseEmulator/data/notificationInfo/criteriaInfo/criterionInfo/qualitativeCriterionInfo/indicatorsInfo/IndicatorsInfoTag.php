<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo\qualitativeCriterionInfo\indicatorsInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractCriteriaDtoClass;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo\qualitativeCriterionInfo\indicatorsInfo\indicatorInfo\IndicatorInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo\qualitativeCriterionInfo\indicatorsInfo\IndicatorsInfoTagInterface;
use DOMDocument;
use DOMNode;

class IndicatorsInfoTag extends AbstractCriteriaDtoClass implements IndicatorsInfoTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $indicatorsInfo = $domTree->createElement('EPtypes:indicatorsInfo');

        foreach ($this->data->getCriteriaIndicators() as $indicator) {
            $indicatorInfo = $this->tagProvider->getTag(IndicatorInfoTagInterface::class);
            $indicatorsInfo->appendChild($indicatorInfo->setData($indicator)->generate($domTree));
        }

        return $indicatorsInfo;
    }
}
