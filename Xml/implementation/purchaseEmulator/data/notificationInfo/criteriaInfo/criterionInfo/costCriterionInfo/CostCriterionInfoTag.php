<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo\costCriterionInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractCriteriaDtoClass;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo\costCriterionInfo\CostCriterionInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo\valueInfo\ValueInfoTagInterface;
use DOMDocument;
use DOMNode;

class CostCriterionInfoTag extends AbstractCriteriaDtoClass implements CostCriterionInfoTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $costCriterionInfo = $domTree->createElement('EPtypes:costCriterionInfo');

        $code = $domTree->createElement('EPtypes:code', $this->data->getCode());
        $costCriterionInfo->appendChild($code);

        $valueInfo = $this->tagProvider->getTag(ValueInfoTagInterface::class);
        $costCriterionInfo->appendChild($valueInfo->setData($this->data)->generate($domTree));

        return $costCriterionInfo;
    }
}
