<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractCriteriaDtoClass;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo\costCriterionInfo\CostCriterionInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo\CriterionInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo\qualitativeCriterionInfo\QualitativeCriterionInfoTagInterface;
use DOMDocument;
use DOMNode;

class CriterionInfoTag extends AbstractCriteriaDtoClass implements CriterionInfoTagInterface
{
    public const CRITERIA_TYPE_INTERFACE = [
        self::COST_CRITERIA => CostCriterionInfoTagInterface::class,
        self::NON_COST_CRITERIA => QualitativeCriterionInfoTagInterface::class,
    ];

    /**
     * @var string
     */
    protected $criteriaType;

    public function generate(DOMDocument $domTree): DOMNode
    {
        $criterionInfo = $domTree->createElement('EPtypes:criterionInfo');

        $typeCriterionInfo = $this->tagProvider->getTag(self::CRITERIA_TYPE_INTERFACE[$this->criteriaType]);
        $criterionInfo->appendChild($typeCriterionInfo->setData($this->data)->generate($domTree));

        return $criterionInfo;
    }

    public function setCriteriaType(string $criteriaType): self
    {
        $this->criteriaType = $criteriaType;
        return $this;
    }
}
