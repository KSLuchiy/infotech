<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo\criteriaInfo;

use common\modules\PurchaseEmulator\dto\request\criteria\CriteriaInfoDto;
use common\modules\Xml\base\tag\AbstractTag;
use common\modules\Xml\base\tag\TagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\criteriaInfo\CriteriaInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo\CriterionInfoTagInterface;
use DOMDocument;
use DOMNode;

class CriteriaInfoTag extends AbstractTag implements CriteriaInfoTagInterface
{
    /**
     * @var CriteriaInfoDto
     */
    protected $data;

    public function setData($data): TagInterface
    {
        if (!$data instanceof CriteriaInfoDto) {
            $this->throwInvalidDataTypeException($data, CriteriaInfoDto::class);
        }

        $this->data = $data;
        return $this;
    }

    public function generate(DOMDocument $domTree): DOMNode
    {
        $criteriaInfo = $domTree->createElement('EPtypes:criteriaInfo');

        $isCertainWorks = $domTree->createElement('EPtypes:isCertainWorks', 'false');
        $criteriaInfo->appendChild($isCertainWorks);

        foreach ($this->data->getCostCriteria()->getIncludedCostCriteria() as $criteria) {
            $criterionInfo = $this->tagProvider->getTag(CriterionInfoTagInterface::class);
            $criteriaInfo->appendChild(
                $criterionInfo->setData($criteria)->setCriteriaType(CriterionInfoTagInterface::COST_CRITERIA)->generate($domTree)
            );
        }

        foreach ($this->data->getNonCostCriteria()->getIncludedNonCostCriteria() as $criteria) {
            $criterionInfo = $this->tagProvider->getTag(CriterionInfoTagInterface::class);
            $criteriaInfo->appendChild(
                $criterionInfo->setData($criteria)->setCriteriaType(CriterionInfoTagInterface::NON_COST_CRITERIA)->generate($domTree)
            );
        }

        return $criteriaInfo;
    }
}
