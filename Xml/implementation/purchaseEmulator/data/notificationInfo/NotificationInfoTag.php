<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\notificationInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractPurchasePublicationDtoTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\contractConditionsInfo\ContractConditionsInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\criteriaInfo\CriteriaInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\CustomerRequirementsInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\flags\FlagsTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\NotificationInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\preferencesInfo\PreferencesInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\procedureInfo\ProcedureInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\purchaseObjectsInfo\PurchaseObjectsInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\requirementsInfo\RequirementsInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\restrictionsInfo\RestrictionsInfoTagInterface;
use DOMDocument;
use DOMException;
use DOMNode;

class NotificationInfoTag extends AbstractPurchasePublicationDtoTag implements NotificationInfoTagInterface
{

    /**
     * @throws DOMException
     */
    public function generate(DOMDocument $domTree): DOMNode
    {
        $notificationInfo = $domTree->createElement('EPtypes:notificationInfo');

        foreach ($this->getCommonTagInterfaces() as $tagInterface) {
            $tag = $this->tagProvider->getTag($tagInterface);
            $notificationInfo->appendChild($tag->setData($this->data)->generate($domTree));
        }

        if ($this->needCriteriaInfoTag()) {
            $criteriaInfo = $this->tagProvider->getTag(CriteriaInfoTagInterface::class);
            $notificationInfo->appendChild($criteriaInfo->setData($this->data->getCriteriaGradesInfo())->generate($domTree));
        }

        if (!empty($this->data->getPreferencesInfo())) {
            $preferencesInfoTag = $this->tagProvider->getTag(PreferencesInfoTagInterface::class);
            $notificationInfo->appendChild($preferencesInfoTag->setData($this->data)->generate($domTree));
        }

        if (!empty($this->data->getRequirementsInfo())) {
            $requirementsInfoTag = $this->tagProvider->getTag(RequirementsInfoTagInterface::class);
            $notificationInfo->appendChild($requirementsInfoTag->setData($this->data)->generate($domTree));
        }

        if (!empty($this->data->getRestrictionsInfo())) {
            $restrictionsInfoTag = $this->tagProvider->getTag(RestrictionsInfoTagInterface::class);
            $notificationInfo->appendChild($restrictionsInfoTag->setData($this->data)->generate($domTree));
        }

        if ($this->needFlagsTag()) {
            $flagsTag = $this->tagProvider->getTag(FlagsTagInterface::class);
            $notificationInfo->appendChild($flagsTag->setData($this->data)->generate($domTree));
        }

        return $notificationInfo;
    }

    /**
     * @return string[]
     */
    protected function getCommonTagInterfaces(): array
    {
        return [
            ProcedureInfoTagInterface::class,
            ContractConditionsInfoTagInterface::class,
            CustomerRequirementsInfoTagInterface::class,
            PurchaseObjectsInfoTagInterface::class
        ];
    }

    protected function needFlagsTag(): bool
    {
        return false;
    }

    protected function needCriteriaInfoTag(): bool
    {
        return false;
    }
}
