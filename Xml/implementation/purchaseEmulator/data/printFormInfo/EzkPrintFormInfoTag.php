<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\printFormInfo;

use common\modules\Xml\base\tag\AbstractFromXmlTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\printFormInfo\PrintFormInfoTagInterface;

class EzkPrintFormInfoTag extends AbstractFromXmlTag implements PrintFormInfoTagInterface
{
    protected function getPathToXml(): string
    {
        return 'purchaseEmulator/ezk/printFormInfo.xml';
    }
}
