<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\purchaseResponsibleInfo\responsibleOrgInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractOrganisationTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\purchaseResponsibleInfo\responsibleOrgInfo\ResponsibleOrgInfoTagInterface;
use DOMDocument;
use DOMException;
use DOMNode;

class ResponsibleOrgInfoTag extends AbstractOrganisationTag implements ResponsibleOrgInfoTagInterface
{
    /**
     * @throws DOMException
     */
    public function generate(DOMDocument $domTree): DOMNode
    {
        $responsibleOrgInfo = $domTree->createElement('EPtypes:responsibleOrgInfo');

        $regNum = $domTree->createElement('EPtypes:regNum', $this->data->getRegNumber());
        $responsibleOrgInfo->appendChild($regNum);

        $consRegistryNum = $domTree->createElement('EPtypes:consRegistryNum', $this->data->getConsRegistryNum());
        $responsibleOrgInfo->appendChild($consRegistryNum);

        $fullName = $domTree->createElement('EPtypes:fullName', $this->data->getFullName());
        $responsibleOrgInfo->appendChild($fullName);

        $postAddress = $domTree->createElement('EPtypes:postAddress', $this->data->getPostAddressLine());
        $responsibleOrgInfo->appendChild($postAddress);

        $factAddress = $domTree->createElement(
            'EPtypes:factAddress',
            $this->data->getFactualAddress() ? $this->data->getFactualAddress()->getAddressLine() : null
        );
        $responsibleOrgInfo->appendChild($factAddress);

        $inn = $domTree->createElement('EPtypes:INN', $this->data->getInn());
        $responsibleOrgInfo->appendChild($inn);

        $kpp = $domTree->createElement('EPtypes:KPP', $this->data->getKpp());
        $responsibleOrgInfo->appendChild($kpp);

        return $responsibleOrgInfo;
    }
}
