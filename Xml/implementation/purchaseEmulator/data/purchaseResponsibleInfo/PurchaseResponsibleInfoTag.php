<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\purchaseResponsibleInfo;

use common\components\App;
use common\Exception\NotFoundException;
use common\models\Entity\Postgres\Organisation\Organisation;
use common\models\Entity\Postgres\RefRecordStatus;
use common\modules\Xml\base\provider\TagProviderInterface;
use common\modules\Xml\dataTypeAwareTag\AbstractPurchasePublicationDtoTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\purchaseResponsibleInfo\PurchaseResponsibleInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\purchaseResponsibleInfo\responsibleInfo\ResponsibleInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\purchaseResponsibleInfo\responsibleOrgInfo\ResponsibleOrgInfoTagInterface;
use common\Repository\Postgres\Organisation\OrganisationRepository;
use Doctrine\ORM\EntityManagerInterface;
use DOMDocument;
use DOMException;
use DOMNode;

class PurchaseResponsibleInfoTag extends AbstractPurchasePublicationDtoTag implements PurchaseResponsibleInfoTagInterface
{
    /**
     * @var OrganisationRepository
     */
    protected $organisationRepository;

    public function __construct(TagProviderInterface $tagProvider)
    {
        parent::__construct($tagProvider);

        /** @var EntityManagerInterface $entityManagerEtp */
        $entityManagerEtp = App::doctrine_pgsql()->getEntityManager();
        $this->organisationRepository = $entityManagerEtp->getRepository(Organisation::class);
    }

    /**
     * @throws DOMException
     * @throws NotFoundException
     */
    public function generate(DOMDocument $domTree): DOMNode
    {
        $organisation = $this->organisationRepository->findOneBy([
            'reg_number' => $this->data->getRegNumberOrganizer(),
            'record_status_id' => RefRecordStatus::ACTIVE,
        ]);

        if ($organisation === null) {
            throw new NotFoundException('Не найдена организация с reg_number = ' . $this->data->getRegNumberOrganizer());
        }

        $purchaseResponsibleInfo = $domTree->createElement('EPtypes:purchaseResponsibleInfo');

        $responsibleOrgInfoTag = $this->tagProvider->getTag(ResponsibleOrgInfoTagInterface::class);
        $purchaseResponsibleInfo->appendChild($responsibleOrgInfoTag->setData($organisation)->generate($domTree));

        $responsibleRole = $domTree->createElement('EPtypes:responsibleRole', 'CU');
        $purchaseResponsibleInfo->appendChild($responsibleRole);

        $responsibleInfoTag = $this->tagProvider->getTag(ResponsibleInfoTagInterface::class);
        $purchaseResponsibleInfo->appendChild($responsibleInfoTag->setData($organisation)->generate($domTree));

        return $purchaseResponsibleInfo;
    }
}
