<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\purchaseResponsibleInfo\responsibleInfo\contactPersonInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractOrganisationTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\purchaseResponsibleInfo\responsibleInfo\contactPersonInfo\ContactPersonInfoTagInterface;
use DOMDocument;
use DOMException;
use DOMNode;

class ContactPersonInfoTag extends AbstractOrganisationTag implements ContactPersonInfoTagInterface
{
    /**
     * @throws DOMException
     */
    public function generate(DOMDocument $domTree): DOMNode
    {
        $contactPersonInfo = $domTree->createElement('EPtypes:contactPersonInfo');

        $lastName = $domTree->createElement('common:lastName', $this->data->getContactLastName());
        $contactPersonInfo->appendChild($lastName);

        $firstName = $domTree->createElement('common:firstName', $this->data->getContactFirstName());
        $contactPersonInfo->appendChild($firstName);

        $middleName = $domTree->createElement('common:middleName', $this->data->getContactMiddleName());
        $contactPersonInfo->appendChild($middleName);

        return $contactPersonInfo;
    }
}
