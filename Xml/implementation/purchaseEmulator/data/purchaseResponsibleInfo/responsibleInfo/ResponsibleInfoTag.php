<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\purchaseResponsibleInfo\responsibleInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractOrganisationTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\purchaseResponsibleInfo\responsibleInfo\contactPersonInfo\ContactPersonInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\purchaseResponsibleInfo\responsibleInfo\ResponsibleInfoTagInterface;
use DOMDocument;
use DOMException;
use DOMNode;

class ResponsibleInfoTag extends AbstractOrganisationTag implements ResponsibleInfoTagInterface
{
    /**
     * @throws DOMException
     */
    public function generate(DOMDocument $domTree): DOMNode
    {
        $responsibleInfo = $domTree->createElement('EPtypes:responsibleInfo');

        $orgPostAddress = $domTree->createElement('EPtypes:orgPostAddress', $this->data->getPostAddressLine());
        $responsibleInfo->appendChild($orgPostAddress);

        $orgFactAddress = $domTree->createElement(
            'EPtypes:orgFactAddress',
            $this->data->getFactualAddress() ? $this->data->getFactualAddress()->getAddressLine() : null
        );
        $responsibleInfo->appendChild($orgFactAddress);

        $contactPersonInfoTag = $this->tagProvider->getTag(ContactPersonInfoTagInterface::class);
        $responsibleInfo->appendChild($contactPersonInfoTag->setData($this->data)->generate($domTree));

        $contactEMail = $domTree->createElement('EPtypes:contactEMail', $this->data->getEmail());
        $responsibleInfo->appendChild($contactEMail);

        $contactPhone = $domTree->createElement('EPtypes:contactPhone', $this->data->getPhone());
        $responsibleInfo->appendChild($contactPhone);

        return $responsibleInfo;
    }
}
