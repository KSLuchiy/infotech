<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\commonInfo;

class EaCommonInfoTag extends CommonInfoTag
{
    protected function getContractConclusionOnSt83Ch2(): string
    {
        return 'purchaseEmulator/false';
    }
}
