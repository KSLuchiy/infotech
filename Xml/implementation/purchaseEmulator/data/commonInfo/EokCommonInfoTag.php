<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\commonInfo;

class EokCommonInfoTag extends CommonInfoTag
{
    protected function IsGozOrNotPublishedOnEISTag(): string
    {
        return 'notPublishedOnEIS';
    }
}
