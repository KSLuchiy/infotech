<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\commonInfo\etp;


use common\modules\Xml\base\tag\AbstractFromXmlTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\commonInfo\etp\EtpTagInterface;

class EtpTag extends AbstractFromXmlTag implements EtpTagInterface
{
    protected function getPathToXml(): string
    {
        return 'purchaseEmulator/common/etp.xml';
    }
}
