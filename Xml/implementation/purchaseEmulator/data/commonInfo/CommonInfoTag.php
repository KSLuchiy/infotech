<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\commonInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractPurchasePublicationDtoTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\commonInfo\CommonInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\commonInfo\etp\EtpTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\commonInfo\placingWay\PlacingWayTagInterface;
use DOMDocument;
use DOMNode;

class CommonInfoTag extends AbstractPurchasePublicationDtoTag implements CommonInfoTagInterface
{
    private const DATE_FORMAT = 'Y-m-dP';
    private const DATE_FORMAT_EIS = 'Y-m-d\TH:i:sP';

    public function generate(DOMDocument $domTree): DOMNode
    {
        $commonInfo = $domTree->createElement('EPtypes:commonInfo');

        $purchaseNumber = $domTree->createElement('EPtypes:purchaseNumber', $this->data->getPurchaseNumber());
        $commonInfo->appendChild($purchaseNumber);

        $docNumber = $domTree->createElement('EPtypes:docNumber', '№' . $this->data->getPurchaseNumber());
        $commonInfo->appendChild($docNumber);

        $plannedPublishDateValue = $this->data->getPublicationDateTime()->format(self::DATE_FORMAT);
        $plannedPublishDate = $domTree->createElement('EPtypes:plannedPublishDate', $plannedPublishDateValue);
        $commonInfo->appendChild($plannedPublishDate);

        $publishDTInEISValue = $this->data->getPublicationDateTime()->format(self::DATE_FORMAT_EIS);
        $publishDTInEIS = $domTree->createElement('EPtypes:publishDTInEIS', $publishDTInEISValue);
        $commonInfo->appendChild($publishDTInEIS);

        $hrefValue = 'https://eis4.roskazna.ru/epz/order/notice/ea20/view/common-info.html?regNumber=7777300041421777801';
        $href = $domTree->createElement('EPtypes:href', $hrefValue);
        $commonInfo->appendChild($href);

        $isGozOrNotPublishedOnEIS = $domTree->createElement('EPtypes:' . $this->IsGozOrNotPublishedOnEISTag(), 'true');
        $commonInfo->appendChild($isGozOrNotPublishedOnEIS);

        $placingWay = $this->tagProvider->getTag(PlacingWayTagInterface::class);
        $commonInfo->appendChild($placingWay->setData($this->data)->generate($domTree));

        $etp = $this->tagProvider->getTag(EtpTagInterface::class);
        $commonInfo->appendChild($etp->setData($this->data)->generate($domTree));

        $contractConclusionOnSt83Ch2 = $domTree->createElement('EPtypes:contractConclusionOnSt83Ch2', $this->getContractConclusionOnSt83Ch2());
        $commonInfo->appendChild($contractConclusionOnSt83Ch2);

        $purchaseObjectInfo = $domTree->createElement('EPtypes:purchaseObjectInfo', $this->data->getPurchaseName());
        $commonInfo->appendChild($purchaseObjectInfo);

        return $commonInfo;
    }

    protected function IsGozOrNotPublishedOnEISTag(): string
    {
        return 'isGOZ';
    }

    protected function getContractConclusionOnSt83Ch2(): string
    {
        return 'true';
    }
}
