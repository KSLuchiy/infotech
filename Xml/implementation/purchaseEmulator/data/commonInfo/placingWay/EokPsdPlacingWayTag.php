<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\commonInfo\placingWay;

use common\modules\Xml\base\tag\AbstractFromXmlTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\commonInfo\placingWay\PlacingWayTagInterface;

class EokPsdPlacingWayTag extends AbstractFromXmlTag implements PlacingWayTagInterface
{
    protected function getPathToXml(): string
    {
        return 'purchaseEmulator/eokPsd/placingWay.xml';
    }
}
