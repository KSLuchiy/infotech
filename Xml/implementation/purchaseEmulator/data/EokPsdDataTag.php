<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data;

use DOMDocument;
use DOMElement;

class EokPsdDataTag extends AbstractDataTag
{
    protected function generateDataTag(DOMDocument $domTree): DOMElement
    {
        $data = $domTree->createElement('integration:data');
        $data->setAttribute('schemeVersion', '11.3');
        return $data;
    }

    protected function getId(): string
    {
        return 93169;
    }
}
