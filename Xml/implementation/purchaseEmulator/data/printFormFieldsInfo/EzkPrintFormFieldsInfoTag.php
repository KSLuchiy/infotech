<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\printFormFieldsInfo;

use common\modules\Xml\base\tag\AbstractFromXmlTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\printFormFieldsInfo\PrintFormFieldsInfoTagInterface;

class EzkPrintFormFieldsInfoTag extends AbstractFromXmlTag implements PrintFormFieldsInfoTagInterface
{
    protected function getPathToXml(): string
    {
        return 'purchaseEmulator/ezk/printFormFieldsInfo.xml';
    }
}
