<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\attachmentsInfo;

use common\modules\Xml\base\tag\AbstractFromXmlTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\attachmentsInfo\AttachmentsInfoTagInterface;

class EokAttachmentsInfoTag extends AbstractFromXmlTag implements AttachmentsInfoTagInterface
{
    protected function getPathToXml(): string
    {
        return 'purchaseEmulator/eok/attachmentsInfo.xml';
    }
}
