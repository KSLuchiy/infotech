<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data\attachmentsInfo;

use common\modules\Xml\base\tag\AbstractFromXmlTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\attachmentsInfo\AttachmentsInfoTagInterface;

class EokPsdAttachmentsInfoTag extends AbstractFromXmlTag implements AttachmentsInfoTagInterface
{
    protected function getPathToXml(): string
    {
        return 'purchaseEmulator/eokPsd/attachmentsInfo.xml';
    }
}
