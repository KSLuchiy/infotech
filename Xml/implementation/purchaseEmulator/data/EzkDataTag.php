<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data;

use common\modules\Xml\interfaces\purchaseEmulator\data\printFormFieldsInfo\PrintFormFieldsInfoTagInterface;
use DOMDocument;
use DOMElement;
use DOMException;
use DOMNode;

class EzkDataTag extends AbstractDataTag
{
    /**
     * @throws DOMException
     */
    protected function generateDataTag(DOMDocument $domTree): DOMElement
    {
        $data = $domTree->createElement('integration:data');
        $data->setAttribute('schemeVersion', '12.0');
        return $data;
    }

    public function generate(DOMDocument $domTree): DOMNode
    {
        $data = parent::generate($domTree);

        $printFormFieldsInfo = $this->tagProvider->getTag(PrintFormFieldsInfoTagInterface::class);
        $data->appendChild($printFormFieldsInfo->setData($this->data)->generate($domTree));

        return $data;
    }

    protected function getId(): string
    {
        return 49677;
    }
}
