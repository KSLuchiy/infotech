<?php

namespace common\modules\Xml\implementation\purchaseEmulator\data;

use common\modules\Xml\dataTypeAwareTag\AbstractPurchasePublicationDtoTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\attachmentsInfo\AttachmentsInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\commonInfo\CommonInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\DataTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\NotificationInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\printFormInfo\PrintFormInfoTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\data\purchaseResponsibleInfo\PurchaseResponsibleInfoTagInterface;
use DOMDocument;
use DOMElement;
use DOMException;
use DOMNode;

abstract class AbstractDataTag extends AbstractPurchasePublicationDtoTag implements DataTagInterface
{
    /**
     * @throws DOMException
     */
    public function generate(DOMDocument $domTree): DOMNode
    {
        $data = $this->generateDataTag($domTree);

        $id = $domTree->createElement('EPtypes:id', $this->getId());
        $data->appendChild($id);

        $versionNumber = $domTree->createElement('EPtypes:versionNumber', '1');
        $data->appendChild($versionNumber);

        $commonInfoTag = $this->tagProvider->getTag(CommonInfoTagInterface::class);
        $data->appendChild($commonInfoTag->setData($this->data)->generate($domTree));

        $purchaseResponsibleTag = $this->tagProvider->getTag(PurchaseResponsibleInfoTagInterface::class);
        $data->appendChild($purchaseResponsibleTag->setData($this->data)->generate($domTree));

        $printFormInfoTag = $this->tagProvider->getTag(PrintFormInfoTagInterface::class);
        $data->appendChild($printFormInfoTag->setData($this->data)->generate($domTree));

        $attachmentsInfoTag = $this->tagProvider->getTag(AttachmentsInfoTagInterface::class);
        $data->appendChild($attachmentsInfoTag->setData($this->data)->generate($domTree));

        $notificationInfoTag = $this->tagProvider->getTag(NotificationInfoTagInterface::class);
        $data->appendChild($notificationInfoTag->setData($this->data)->generate($domTree));

        return $data;
    }

    abstract protected function generateDataTag(DOMDocument $domTree): DOMElement;

    abstract protected function getId(): string;
}
