<?php

namespace common\modules\Xml\implementation\purchaseEmulator\index;

use common\modules\Xml\dataTypeAwareTag\AbstractPurchasePublicationDtoTag;
use common\modules\Xml\interfaces\purchaseEmulator\index\IndexTagInterface;
use DOMDocument;
use DOMException;
use DOMNode;
use function common\helpers\generateGuid;

abstract class AbstractIndexTag extends AbstractPurchasePublicationDtoTag implements IndexTagInterface
{
    private const DATETIME_FORMAT = 'Y-m-d\TH:i:s';

    /**
     * @throws DOMException
     */
    public function generate(DOMDocument $domTree): DOMNode
    {
        $index = $domTree->createElement('integration:index');

        $id = $domTree->createElement('integration:id', generateGuid());
        $index->appendChild($id);

        $sender = $domTree->createElement('integration:sender', 'OOC');
        $index->appendChild($sender);

        $receiver = $domTree->createElement('integration:receiver', 'ETP_RAD');
        $index->appendChild($receiver);

        $createDateTime = $domTree->createElement(
            'integration:createDateTime',
            $this->data->getPublicationDateTime()->format(self::DATETIME_FORMAT)
        );

        $index->appendChild($createDateTime);

        $objectType = $domTree->createElement('integration:objectType', $this->getObjectType());
        $index->appendChild($objectType);

        $objectId = $domTree->createElement('integration:objectId', $this->data->getPurchaseNumber());
        $index->appendChild($objectId);

        $indexNum = $domTree->createElement('integration:indexNum', '1');
        $index->appendChild($indexNum);

        $signature = $domTree->createElement('integration:signature');
        $signature->setAttribute('type', 'CAdES-BES');
        $index->appendChild($signature);

        $mode = $domTree->createElement('integration:mode', 'TEST');
        $index->appendChild($mode);

        return $index;
    }

    abstract protected function getObjectType(): string;
}
