<?php

namespace common\modules\Xml\implementation\purchaseEmulator\index;

class EaIndexTag extends AbstractIndexTag
{
    protected function getObjectType(): string
    {
        return 'EF';
    }
}
