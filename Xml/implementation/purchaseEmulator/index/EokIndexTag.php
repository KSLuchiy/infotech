<?php

namespace common\modules\Xml\implementation\purchaseEmulator\index;

class EokIndexTag extends AbstractIndexTag
{
    protected function getObjectType(): string
    {
        return 'EOK';
    }
}
