<?php

namespace common\modules\Xml\implementation\purchaseEmulator\index;

class EzkIndexTag extends AbstractIndexTag
{
    protected function getObjectType(): string
    {
        return 'EZK';
    }
}
