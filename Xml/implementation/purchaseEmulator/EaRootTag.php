<?php

namespace common\modules\Xml\implementation\purchaseEmulator;

class EaRootTag extends AbstractRootTag
{
    protected function getRootTagName(): string
    {
        return 'epNotificationEF2020';
    }
}
