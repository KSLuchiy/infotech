<?php

namespace common\modules\Xml\implementation\purchaseEmulator;

class EokRootTag extends AbstractRootTag
{
    protected function getRootTagName(): string
    {
        return 'epNotificationEOK2020';
    }
}
