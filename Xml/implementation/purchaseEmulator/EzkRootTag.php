<?php

namespace common\modules\Xml\implementation\purchaseEmulator;

class EzkRootTag extends AbstractRootTag
{
    protected function getRootTagName(): string
    {
        return 'epNotificationEZK2020';
    }
}
