<?php

namespace common\modules\Xml\implementation\purchaseEmulator;

use common\Exception\RuntimeException;
use common\modules\Xml\base\tag\RootTagInterface;
use common\modules\Xml\dataTypeAwareTag\AbstractPurchasePublicationDtoTag;
use common\modules\Xml\interfaces\purchaseEmulator\data\DataTagInterface;
use common\modules\Xml\interfaces\purchaseEmulator\index\IndexTagInterface;
use DOMDocument;
use DOMElement;
use DOMException;
use DOMNode;

abstract class AbstractRootTag extends AbstractPurchasePublicationDtoTag implements RootTagInterface
{
    private const NAMESPACES = [
        'base' => 'http://zakupki.gov.ru/oos/base/1',
        'common' => 'http://zakupki.gov.ru/oos/common/1',
        'integration' => 'http://zakupki.gov.ru/oos/integration/1',
        'EPtypes' => 'http://zakupki.gov.ru/oos/EPtypes/1',
    ];

    /**
     * @throws DOMException
     */
    public function generate(DOMDocument $domTree): DOMNode
    {
        $root = $domTree->createElementNS(self::NAMESPACES['integration'], 'integration:' . $this->getRootTagName());
        if (!$root) {
            throw new RuntimeException('Ошибка создания корневого тега');
        }

        $this->addNamespaces($root);

        $indexTag = $this->tagProvider->getTag(IndexTagInterface::class);
        $root->appendChild($indexTag->setData($this->data)->generate($domTree));

        $dataTag = $this->tagProvider->getTag(DataTagInterface::class);
        $root->appendChild($dataTag->setData($this->data)->generate($domTree));

        return $root;
    }

    private function addNamespaces(DOMElement $root): void
    {
        foreach (self::NAMESPACES as $prefix => $href) {
            $root->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:' . $prefix, $href);
        }
    }

    abstract protected function getRootTagName(): string;
}
