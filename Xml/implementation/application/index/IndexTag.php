<?php

namespace common\modules\Xml\implementation\application\index;

use common\modules\Xml\dataTypeAwareTag\AbstractApplicationTag;
use common\modules\Xml\interfaces\pprf615ContractSign\index\IndexTagInterface;
use DateTime;
use DateTimeZone;
use DOMDocument;
use DOMNode;
use function common\helpers\generateGuid;

class IndexTag extends AbstractApplicationTag implements IndexTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $index = $domTree->createElement('index');

        $id = $domTree->createElement('id', generateGuid());
        $index->appendChild($id);

        $sender = $domTree->createElement('sender', 'ETP_RAD');
        $index->appendChild($sender);

        $receiver = $domTree->createElement('receiver', 'OOC');
        $index->appendChild($receiver);

        $currentDateTimeUtcFormatted = (new DateTime())
            ->setTimezone(new DateTimeZone('UTC'))
            ->format('Y-m-d\TH:i:s\Z');

        $createDateTime = $domTree->createElement('createDateTime', $currentDateTimeUtcFormatted);
        $index->appendChild($createDateTime);

        $objectType = $domTree->createElement('objectType', 'EP_DD');
        $index->appendChild($objectType);

        $objectId = $domTree->createElement('objectId', $this->data->getPurchaseNumber());
        $index->appendChild($objectId);

        $indexNum = $domTree->createElement('indexNum', 1);
        $index->appendChild($indexNum);

        $signature = $domTree->createElement('signature');
        $signature->setAttribute('type', 'CAdES-BES');
        $index->appendChild($signature);

        $mode = $domTree->createElement('mode', getenv('EIS_INTEGRATION_MODE'));
        $index->appendChild($mode);

        return $index;
    }
}
