<?php

namespace common\modules\Xml\implementation\application\data\attachmentsInfo\attachmentInfo\cryptoSigns;

use common\modules\Xml\dataTypeAwareTag\AbstractApplicationFileTag;
use common\modules\Xml\interfaces\application\data\attachmentsInfo\attachmentInfo\cryptoSigns\CryptoSignsTagInterface;
use DOMDocument;
use DOMNode;

class CryptoSignsTag extends AbstractApplicationFileTag implements CryptoSignsTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $cryptoSigns = $domTree->createElement('cryptoSigns');

        $signature = $domTree->createElement('signature', $this->data->getSignature());
        $signature->setAttribute('xmlns', 'http://zakupki.gov.ru/oos/pprf615types/1');
        $signature->setAttribute('type', 'CAdES-BES');

        $cryptoSigns->appendChild($signature);

        return $cryptoSigns;
    }
}