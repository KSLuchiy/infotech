<?php

namespace common\modules\Xml\implementation\application\data\attachmentsInfo\attachmentInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractApplicationFileTag;
use common\modules\Xml\interfaces\application\data\attachmentsInfo\attachmentInfo\cryptoSigns\CryptoSignsTagInterface;
use common\modules\Xml\interfaces\pprf615ContractSign\data\attachmentsInfo\attachmentInfo\AttachmentInfoTagInterface;
use DOMDocument;
use DOMNode;

class AttachmentInfo extends AbstractApplicationFileTag implements AttachmentInfoTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $attachmentInfo = $domTree->createElement('attachmentInfo');
        $attachmentInfo->setAttribute('xmlns', 'http://zakupki.gov.ru/oos/pprf615types/1');

        $fileName = $domTree->createElement('fileName', $this->data->getName());
        $attachmentInfo->appendChild($fileName);

        $content = $domTree->createElement(
            'content',
            base64_encode(file_get_contents(sprintf('%s/%s', $this->data->getPath(), $this->data->getHash())))
        );
        $content->setAttribute('xmlns', 'http://zakupki.gov.ru/oos/pprf615types/1');
        $attachmentInfo->appendChild($content);

        $cryptoSigns = $this->tagProvider->getTag(CryptoSignsTagInterface::class);
        $attachmentInfo->appendChild($cryptoSigns->setData($this->data)->generate($domTree));

        return $attachmentInfo;
    }
}