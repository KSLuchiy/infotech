<?php

namespace common\modules\Xml\implementation\application\data\attachmentsInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractApplicationTag;
use common\modules\Xml\interfaces\application\data\attachmentsInfo\attachmentInfo\AttachmentInfoTagInterface;
use common\modules\Xml\interfaces\application\data\attachmentsInfo\AttachmentsInfoTagInterface;
use DOMDocument;
use DOMNode;

class AttachmentsInfoTag extends AbstractApplicationTag implements AttachmentsInfoTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $attachmentsInfo = $domTree->createElement('attachmentsInfo');
        $attachmentsInfo->setAttribute('xmlns', 'http://zakupki.gov.ru/oos/pprf615types/1');

        foreach ($this->data->getDocDocument()->getFiles() as $file) {
            if (file_exists(sprintf('%s/%s', $file->getPath(), $file->getHash()))) {
                continue;
            }

            $attachmentInfo = $this->tagProvider->getTag(AttachmentInfoTagInterface::class);
            $attachmentsInfo->appendChild($attachmentInfo->setData($file)->generate($domTree));
        }

        return $attachmentsInfo;
    }
}