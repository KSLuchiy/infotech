<?php

namespace common\modules\Xml\implementation\application\data\participantInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractApplicationTag;
use common\modules\Xml\interfaces\application\data\participantInfo\ParticipantInfoTagInterface;
use DOMDocument;
use DOMNode;

class ParticipantInfoTag extends AbstractApplicationTag implements ParticipantInfoTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $participantInfo = $domTree->createElement('participantInfo');
        $participantInfo->setAttribute('xmlns', 'http://zakupki.gov.ru/oos/pprf615types/1');

        $name = $domTree->createElement('name', $this->data->getSupplier()->getFullName());
        $participantInfo->appendChild($name);

        $email = $domTree->createElement('email', $this->data->getSupplier()->getEmail());
        $participantInfo->appendChild($email);

        return $participantInfo;
    }
}
