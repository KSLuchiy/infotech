<?php

namespace common\modules\Xml\implementation\application\data;

use common\modules\Xml\dataTypeAwareTag\AbstractApplicationTag;
use common\modules\Xml\implementation\application\data\attachmentsInfo\AttachmentsInfoTag;
use common\modules\Xml\implementation\application\data\commonInfo\CommonInfoTag;
use common\modules\Xml\implementation\application\data\participantInfo\ParticipantInfoTag;
use common\modules\Xml\interfaces\application\data\DataTagInterface;
use DOMDocument;
use DOMNode;

class DataTag extends AbstractApplicationTag implements DataTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $data = $domTree->createElement('data');
        $data->setAttribute('schemeVersion', '9.0');

        $externalId = $domTree->createElement('externalId', 'index/id'); //TODO: получить из index/id
        $externalId->setAttribute('xmlns', 'http://zakupki.gov.ru/oos/pprf615types/1');
        $data->appendChild($externalId);

        $commonInfo = $this->tagProvider->getTag(CommonInfoTag::class);
        $data->appendChild($commonInfo->setData($this->data)->generate($domTree));

        $participantInfo = $this->tagProvider->getTag(ParticipantInfoTag::class);
        $data->appendChild($participantInfo->setData($this->data)->generate($domTree));

        $attachmentsInfo = $this->tagProvider->getTag(AttachmentsInfoTag::class);
        $data->appendChild($attachmentsInfo->setData($this->data)->generate($domTree));

        return $data;
    }
}
