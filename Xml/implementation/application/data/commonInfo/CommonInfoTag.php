<?php

namespace common\modules\Xml\implementation\application\data\commonInfo;

use common\modules\Xml\dataTypeAwareTag\AbstractApplicationTag;
use common\modules\Xml\interfaces\application\data\commonInfo\CommonInfoTagInterface;
use DateTime;
use DOMDocument;
use DOMNode;

class CommonInfoTag extends AbstractApplicationTag implements CommonInfoTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $commonInfo = $domTree->createElement('commonInfo');
        $commonInfo->setAttribute('xmlns', 'http://zakupki.gov.ru/oos/pprf615types/1');

        $purchaseNumber = $domTree->createElement('purchaseNumber', $this->data->getPurchaseNumber());
        $commonInfo->appendChild($purchaseNumber);

        $docNumber = $domTree->createElement('docNumber', 'purchaseNumber + string');
        $commonInfo->appendChild($docNumber); //TODO: спросить у аналитики откуда номер

        $docDate = $domTree->createElement('docDate', (new DateTime())->format('Y-m-d'));
        $commonInfo->appendChild($docDate);

        $topic = $domTree->createElement('topic', 'find topic');
        $commonInfo->appendChild($topic); //TODO: понять откуда топик

        return $commonInfo;
    }
}
