<?php

namespace common\modules\Xml\implementation\application;

use common\modules\Xml\dataTypeAwareTag\AbstractContractTag;
use common\modules\Xml\interfaces\application\Pprf615ClarificationRequestTagInterface;
use common\modules\Xml\interfaces\pprf615ContractSign\data\DataTagInterface;
use common\modules\Xml\interfaces\pprf615ContractSign\index\IndexTagInterface;
use DOMDocument;
use DOMNode;

class Pprf615ClarificationRequest extends AbstractContractTag implements Pprf615ClarificationRequestTagInterface
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $pprf615ClarificationRequest = $domTree->createElement('pprf615ClarificationRequest');
        $pprf615ClarificationRequest->setAttribute('xmlns', 'http://zakupki.gov.ru/oos/integration/1');

        $indexTag = $this->tagProvider->getTag(IndexTagInterface::class);
        $pprf615ClarificationRequest->appendChild($indexTag->setData($this->data)->generate($domTree));

        $dataTag = $this->tagProvider->getTag(DataTagInterface::class);
        $pprf615ClarificationRequest->appendChild($dataTag->setData($this->data)->generate($domTree));

        return $pprf615ClarificationRequest;
    }
}
