<?php

return [
    common\models\Entity\Postgres\Purchase::PLACING_WAY_CODE_EA615 => [
        \common\modules\Xml\implementation\application\Pprf615ClarificationRequest::class,
        \common\modules\Xml\implementation\application\index\IndexTag::class,
        \common\modules\Xml\implementation\application\data\DataTag::class,
        \common\modules\Xml\implementation\application\data\commonInfo\CommonInfoTag::class,
        \common\modules\Xml\implementation\application\data\participantInfo\ParticipantInfoTag::class,
        \common\modules\Xml\implementation\application\data\attachmentsInfo\AttachmentsInfoTag::class,
        \common\modules\Xml\implementation\application\data\attachmentsInfo\attachmentInfo\AttachmentInfo::class,
        \common\modules\Xml\implementation\application\data\attachmentsInfo\attachmentInfo\cryptoSigns\CryptoSignsTag::class,
    ],
];
