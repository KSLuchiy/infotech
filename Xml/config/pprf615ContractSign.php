<?php

return [
    \common\modules\Xml\implementation\pprf615ContractSign\Pprf615ContractSignTag::class,
    \common\modules\Xml\implementation\pprf615ContractSign\index\IndexTag::class,
    \common\modules\Xml\implementation\pprf615ContractSign\data\DataTag::class,
    \common\modules\Xml\implementation\pprf615ContractSign\data\attachmentsInfo\AttachmentsInfoTag::class,
    \common\modules\Xml\implementation\pprf615ContractSign\data\attachmentsInfo\attachmentInfo\AttachmentInfoTag::class,
    \common\modules\Xml\implementation\pprf615ContractSign\data\attachmentsInfo\attachmentInfo\cryptoSigns\CryptoSignsTag::class,
    \common\modules\Xml\implementation\pprf615ContractSign\data\commonInfo\CommonInfoTag::class,
    \common\modules\Xml\implementation\pprf615ContractSign\data\commonInfo\purchaseInfo\PurchaseInfoTag::class,
    \common\modules\Xml\implementation\pprf615ContractSign\data\contractorInfo\ContractorInfoTag::class,
    \common\modules\Xml\implementation\pprf615ContractSign\data\contractorInfo\individualPersonInfo\IndividualPersonInfoTag::class,
    \common\modules\Xml\implementation\pprf615ContractSign\data\contractorInfo\individualPersonInfo\nameInfo\NameInfoTag::class,
    \common\modules\Xml\implementation\pprf615ContractSign\data\contractorInfo\legalEntityForeignStateInfo\LegaEntityForeignStateInfoTag::class,
    \common\modules\Xml\implementation\pprf615ContractSign\data\contractorInfo\legalEntityForeignStateInfo\countryInfo\CountryInfoTag::class,
    \common\modules\Xml\implementation\pprf615ContractSign\data\contractorInfo\legalEntityRFInfo\LegalEntityRfInfoTag::class,
    \common\modules\Xml\implementation\pprf615ContractSign\data\contractorInfo\legalEntityRFInfo\contactPerson\ContactPersonTag::class,
    \common\modules\Xml\implementation\pprf615ContractSign\data\customerInfo\CustomerInfoTag::class,
    \common\modules\Xml\implementation\pprf615ContractSign\data\extPrintFormInfo\ExtPrintFormInfoTag::class,
    \common\modules\Xml\implementation\pprf615ContractSign\data\financesInfo\FinancesInfoTag::class,
];
