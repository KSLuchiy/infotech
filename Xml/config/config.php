<?php

return [
    'params' => [
        \common\modules\Xml\provider\Pprf615ContractSignTagProvider::CONFIG_KEY => require 'pprf615ContractSign.php',
        \common\modules\Xml\provider\PurchaseEmulatorTagProvider::CONFIG_KEY => require 'purchaseEmulator.php',
        \common\modules\Xml\provider\ApplicationTagProvider::CONFIG_KEY => require 'application.php',
    ]
];
