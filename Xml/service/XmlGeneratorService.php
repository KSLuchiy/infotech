<?php

namespace common\modules\Xml\service;

use common\modules\Xml\base\provider\TagProviderInterface;
use common\modules\Xml\base\tag\RootTagInterface;
use DOMDocument;
use DOMException;

class XmlGeneratorService
{
    /**
     * @param mixed $data
     * @throws DOMException
     */
    public function generate(TagProviderInterface $tagProvider, $data): string
    {
        $domTree = new DOMDocument('1.0', 'UTF-8');
        $domTree->preserveWhiteSpace = false;
        $domTree->formatOutput = true;

        $rootTag = $tagProvider->getTag(RootTagInterface::class);
        $domTree->appendChild($rootTag->setData($data)->generate($domTree));

        return $domTree->saveXML();
    }
}
