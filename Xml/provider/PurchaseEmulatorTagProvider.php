<?php

namespace common\modules\Xml\provider;

use common\Exception\InvalidArgumentException;
use common\modules\Xml\base\provider\AbstractConfigTagProvider;
use common\modules\Xml\XmlModule;
use Yii;

class PurchaseEmulatorTagProvider extends AbstractConfigTagProvider
{
    public const CONFIG_KEY = 'purchase-emulator';

    /**
     * @var string[]
     */
    private $tagClasses;

    public function __construct(string $discriminator)
    {
        $config = Yii::$app->getModule(XmlModule::MODULE_NAME)->params[self::CONFIG_KEY];
        if (empty($config[$discriminator])) {
            throw new InvalidArgumentException('Не найдены теги в config.php для дискриминатора = ' . $discriminator);
        }

        $this->tagClasses = array_merge($config['common'], $config[$discriminator]);
    }

    protected function getTagClasses(): array
    {
        return $this->tagClasses;
    }
}
