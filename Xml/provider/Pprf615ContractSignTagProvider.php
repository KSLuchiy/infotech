<?php

namespace common\modules\Xml\provider;

use common\modules\Xml\base\provider\AbstractConfigTagProvider;
use common\modules\Xml\XmlModule;
use Yii;

class Pprf615ContractSignTagProvider extends AbstractConfigTagProvider
{
    public const CONFIG_KEY = 'pprf615ContractSign';

    /**
     * @var string[]
     */
    private $tagClasses;

    public function __construct()
    {
        $this->tagClasses = Yii::$app->getModule(XmlModule::MODULE_NAME)->params[self::CONFIG_KEY];
    }

    protected function getTagClasses(): array
    {
        return $this->tagClasses;
    }
}
