<?php

namespace common\modules\Xml\interfaces\pprf615ContractSign;

use common\modules\Xml\base\tag\RootTagInterface;

interface Pprf615ContractSignTagInterface extends RootTagInterface
{

}
