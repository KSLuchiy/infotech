<?php

namespace common\modules\Xml\interfaces\pprf615ContractSign\data\commonInfo;

use common\modules\Xml\base\tag\TagInterface;

interface CommonInfoTagInterface extends TagInterface
{

}
