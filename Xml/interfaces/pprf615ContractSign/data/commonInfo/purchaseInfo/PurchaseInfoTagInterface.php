<?php

namespace common\modules\Xml\interfaces\pprf615ContractSign\data\commonInfo\purchaseInfo;

use common\modules\Xml\base\tag\TagInterface;

interface PurchaseInfoTagInterface extends TagInterface
{

}
