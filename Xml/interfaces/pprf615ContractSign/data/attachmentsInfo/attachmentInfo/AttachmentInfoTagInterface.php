<?php

namespace common\modules\Xml\interfaces\pprf615ContractSign\data\attachmentsInfo\attachmentInfo;

use common\modules\Xml\base\tag\TagInterface;

interface AttachmentInfoTagInterface extends TagInterface
{

}
