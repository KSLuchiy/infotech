<?php

namespace common\modules\Xml\interfaces\pprf615ContractSign\data\attachmentsInfo\attachmentInfo\cryptoSigns;

use common\modules\Xml\base\tag\TagInterface;

interface CryptoSignsTagInterface extends TagInterface
{

}
