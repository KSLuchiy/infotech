<?php

namespace common\modules\Xml\interfaces\pprf615ContractSign\data\attachmentsInfo;

use common\modules\Xml\base\tag\TagInterface;

interface AttachmentsInfoTagInterface extends TagInterface
{

}
