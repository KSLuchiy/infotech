<?php

namespace common\modules\Xml\interfaces\pprf615ContractSign\data\contractorInfo\individualPersonInfo;

use common\modules\Xml\base\tag\TagInterface;

interface IndividualPersonInfoTagInterface extends TagInterface
{

}
