<?php

namespace common\modules\Xml\interfaces\pprf615ContractSign\data\contractorInfo\individualPersonInfo\nameInfo;

use common\modules\Xml\base\tag\TagInterface;

interface NameInfoTagInterface extends TagInterface
{

}
