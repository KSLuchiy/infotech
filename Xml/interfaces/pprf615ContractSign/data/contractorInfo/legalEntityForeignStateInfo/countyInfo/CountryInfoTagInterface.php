<?php

namespace common\modules\Xml\interfaces\pprf615ContractSign\data\contractorInfo\legalEntityForeignStateInfo\countyInfo;

use common\modules\Xml\base\tag\TagInterface;

interface CountryInfoTagInterface extends TagInterface
{

}
