<?php

namespace common\modules\Xml\interfaces\pprf615ContractSign\data\contractorInfo\legalEntityForeignStateInfo;

use common\modules\Xml\base\tag\TagInterface;

interface LegalEntityForeignStateInfoTagInterface extends TagInterface
{

}
