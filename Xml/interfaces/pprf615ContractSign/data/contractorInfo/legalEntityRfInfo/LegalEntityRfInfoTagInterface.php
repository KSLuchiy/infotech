<?php

namespace common\modules\Xml\interfaces\pprf615ContractSign\data\contractorInfo\legalEntityRfInfo;

use common\modules\Xml\base\tag\TagInterface;

interface LegalEntityRfInfoTagInterface extends TagInterface
{

}
