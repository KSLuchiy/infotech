<?php

namespace common\modules\Xml\interfaces\pprf615ContractSign\data\contractorInfo\legalEntityRfInfo\contactPerson;

use common\modules\Xml\base\tag\TagInterface;

interface ContactPersonTagInterface extends TagInterface
{

}
