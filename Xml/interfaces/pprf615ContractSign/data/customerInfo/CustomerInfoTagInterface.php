<?php

namespace common\modules\Xml\interfaces\pprf615ContractSign\data\customerInfo;

use common\modules\Xml\base\tag\TagInterface;

interface CustomerInfoTagInterface extends TagInterface
{

}
