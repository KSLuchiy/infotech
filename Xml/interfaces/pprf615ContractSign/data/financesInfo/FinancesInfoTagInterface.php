<?php

namespace common\modules\Xml\interfaces\pprf615ContractSign\data\financesInfo;

use common\modules\Xml\base\tag\TagInterface;

interface FinancesInfoTagInterface extends TagInterface
{

}
