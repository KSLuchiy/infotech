<?php

namespace common\modules\Xml\interfaces\pprf615ContractSign\data;

use common\modules\Xml\base\tag\TagInterface;

interface DataTagInterface extends TagInterface
{

}
