<?php

namespace common\modules\Xml\interfaces\pprf615ContractSign\data\extPrintFormInfo;

use common\modules\Xml\base\tag\TagInterface;

interface ExtPrintFormInfoTagInterface extends TagInterface
{

}
