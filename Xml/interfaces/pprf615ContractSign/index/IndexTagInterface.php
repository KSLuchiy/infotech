<?php

namespace common\modules\Xml\interfaces\pprf615ContractSign\index;

use common\modules\Xml\base\tag\TagInterface;

interface IndexTagInterface extends TagInterface
{

}
