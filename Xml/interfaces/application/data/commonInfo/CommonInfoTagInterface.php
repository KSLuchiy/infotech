<?php

namespace common\modules\Xml\interfaces\application\data\commonInfo;

use common\modules\Xml\base\tag\TagInterface;

interface CommonInfoTagInterface extends TagInterface
{

}
