<?php

namespace common\modules\Xml\interfaces\application\data\participantInfo;

use common\modules\Xml\base\tag\TagInterface;

interface ParticipantInfoTagInterface extends TagInterface
{

}
