<?php

namespace common\modules\Xml\interfaces\application\data;

use common\modules\Xml\base\tag\TagInterface;

interface DataTagInterface extends TagInterface
{

}
