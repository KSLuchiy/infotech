<?php

namespace common\modules\Xml\interfaces\application\data\attachmentsInfo\attachmentInfo\cryptoSigns;

use common\modules\Xml\base\tag\TagInterface;

interface CryptoSignsTagInterface extends TagInterface
{

}