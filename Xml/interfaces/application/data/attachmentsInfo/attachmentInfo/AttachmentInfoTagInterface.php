<?php

namespace common\modules\Xml\interfaces\application\data\attachmentsInfo\attachmentInfo;

use common\modules\Xml\base\tag\TagInterface;

interface AttachmentInfoTagInterface extends TagInterface
{

}