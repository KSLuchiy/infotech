<?php

namespace common\modules\Xml\interfaces\application\index;

use common\modules\Xml\base\tag\TagInterface;

interface IndexTagInterface extends TagInterface
{

}
