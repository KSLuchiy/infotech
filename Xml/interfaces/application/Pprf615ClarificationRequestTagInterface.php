<?php

namespace common\modules\Xml\interfaces\application;

use common\modules\Xml\base\tag\RootTagInterface;

interface Pprf615ClarificationRequestTagInterface extends RootTagInterface
{

}
