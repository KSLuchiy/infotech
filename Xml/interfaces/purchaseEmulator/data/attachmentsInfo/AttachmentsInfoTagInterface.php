<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\attachmentsInfo;

use common\modules\Xml\base\tag\TagInterface;

interface AttachmentsInfoTagInterface extends TagInterface
{

}
