<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\printFormInfo;

use common\modules\Xml\base\tag\TagInterface;

interface PrintFormInfoTagInterface extends TagInterface
{

}
