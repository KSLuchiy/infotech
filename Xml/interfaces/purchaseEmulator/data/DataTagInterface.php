<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data;

use common\modules\Xml\base\tag\TagInterface;

interface DataTagInterface extends TagInterface
{

}
