<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\printFormFieldsInfo;

use common\modules\Xml\base\tag\TagInterface;

interface PrintFormFieldsInfoTagInterface extends TagInterface
{

}
