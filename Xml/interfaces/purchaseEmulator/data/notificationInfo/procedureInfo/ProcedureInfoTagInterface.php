<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\procedureInfo;

use common\modules\Xml\base\tag\TagInterface;

interface ProcedureInfoTagInterface extends TagInterface
{

}
