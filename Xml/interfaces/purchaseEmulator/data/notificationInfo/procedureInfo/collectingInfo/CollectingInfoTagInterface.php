<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\procedureInfo\collectingInfo;

use common\modules\Xml\base\tag\TagInterface;

interface CollectingInfoTagInterface extends TagInterface
{

}
