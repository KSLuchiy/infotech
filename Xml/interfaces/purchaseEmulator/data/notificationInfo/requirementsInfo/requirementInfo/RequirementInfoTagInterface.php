<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\requirementsInfo\requirementInfo;

use common\modules\Xml\base\tag\TagInterface;

interface RequirementInfoTagInterface extends TagInterface
{

}
