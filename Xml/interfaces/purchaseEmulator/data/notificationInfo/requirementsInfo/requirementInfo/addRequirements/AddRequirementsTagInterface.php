<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\requirementsInfo\requirementInfo\addRequirements;

use common\modules\Xml\base\tag\TagInterface;

interface AddRequirementsTagInterface extends TagInterface
{

}
