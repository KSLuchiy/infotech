<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\requirementsInfo\requirementInfo\addRequirements\addRequirement;

use common\modules\Xml\base\tag\TagInterface;

interface AddRequirementTagInterface extends TagInterface
{

}
