<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\requirementsInfo;

use common\modules\Xml\base\tag\TagInterface;

interface RequirementsInfoTagInterface extends TagInterface
{

}
