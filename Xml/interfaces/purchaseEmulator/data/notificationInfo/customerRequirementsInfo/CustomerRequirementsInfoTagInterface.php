<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo;

use common\modules\Xml\base\tag\TagInterface;

interface CustomerRequirementsInfoTagInterface extends TagInterface
{

}
