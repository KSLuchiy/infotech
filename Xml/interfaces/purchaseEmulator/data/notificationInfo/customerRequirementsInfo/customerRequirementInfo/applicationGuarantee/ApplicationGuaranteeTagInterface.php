<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\applicationGuarantee;

use common\modules\Xml\base\tag\TagInterface;

interface ApplicationGuaranteeTagInterface extends TagInterface
{

}
