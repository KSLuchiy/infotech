<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo;

use common\modules\Xml\base\tag\TagInterface;

interface CustomerRequirementInfoTagInterface extends TagInterface
{

}
