<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractConditionsInfo;

use common\modules\Xml\base\tag\TagInterface;

interface ContractConditionsInfoTagInterface extends TagInterface
{

}
