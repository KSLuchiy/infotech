<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractConditionsInfo\deliveryPlacesInfo;

use common\modules\Xml\base\tag\TagInterface;

interface DeliveryPlacesInfoTagInterface extends TagInterface
{

}
