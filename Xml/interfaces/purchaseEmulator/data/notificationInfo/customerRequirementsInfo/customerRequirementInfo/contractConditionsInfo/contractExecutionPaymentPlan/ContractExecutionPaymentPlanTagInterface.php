<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractConditionsInfo\contractExecutionPaymentPlan;

use common\modules\Xml\base\tag\TagInterface;

interface ContractExecutionPaymentPlanTagInterface extends TagInterface
{

}
