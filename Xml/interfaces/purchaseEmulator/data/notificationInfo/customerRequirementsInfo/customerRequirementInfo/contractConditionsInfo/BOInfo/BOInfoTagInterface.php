<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractConditionsInfo\BOInfo;

use common\modules\Xml\base\tag\TagInterface;

interface BOInfoTagInterface extends TagInterface
{

}
