<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractConditionsInfo\ikzInfo;

use common\modules\Xml\base\tag\TagInterface;

interface IkzInfoTagInterface extends TagInterface
{

}
