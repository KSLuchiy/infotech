<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\contractGuarantee;

use common\modules\Xml\base\tag\TagInterface;

interface ContractGuaranteeTagInterface extends TagInterface
{

}
