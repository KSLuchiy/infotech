<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\customer;

use common\modules\Xml\base\tag\TagInterface;

interface CustomerTagInterface extends TagInterface
{

}
