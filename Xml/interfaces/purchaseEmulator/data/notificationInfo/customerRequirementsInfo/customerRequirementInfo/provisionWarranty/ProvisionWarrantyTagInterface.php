<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\customerRequirementsInfo\customerRequirementInfo\provisionWarranty;

use common\modules\Xml\base\tag\TagInterface;

interface ProvisionWarrantyTagInterface extends TagInterface
{

}
