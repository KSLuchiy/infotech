<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\purchaseObjectsInfo\notDrugPurchaseObjectsInfo\purchaseObject;

use common\modules\Xml\base\tag\TagInterface;

interface PurchaseObjectTagInterface extends TagInterface
{

}
