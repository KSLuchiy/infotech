<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\purchaseObjectsInfo\notDrugPurchaseObjectsInfo\purchaseObject\quantity;

use common\modules\Xml\base\tag\TagInterface;

interface QuantityTagInterface extends TagInterface
{

}
