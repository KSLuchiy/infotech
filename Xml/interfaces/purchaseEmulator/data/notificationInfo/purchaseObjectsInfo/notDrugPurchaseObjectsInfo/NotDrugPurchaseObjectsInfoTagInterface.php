<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\purchaseObjectsInfo\notDrugPurchaseObjectsInfo;

use common\modules\Xml\base\tag\TagInterface;

interface NotDrugPurchaseObjectsInfoTagInterface extends TagInterface
{

}
