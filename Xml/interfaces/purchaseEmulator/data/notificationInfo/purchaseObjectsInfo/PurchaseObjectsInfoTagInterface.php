<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\purchaseObjectsInfo;

use common\modules\Xml\base\tag\TagInterface;

interface PurchaseObjectsInfoTagInterface extends TagInterface
{

}
