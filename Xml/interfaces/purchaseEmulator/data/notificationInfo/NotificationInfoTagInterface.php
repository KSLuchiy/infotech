<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo;

use common\modules\Xml\base\tag\TagInterface;

interface NotificationInfoTagInterface extends TagInterface
{

}
