<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\preferencesInfo\preferenceInfo;

use common\modules\Xml\base\tag\TagInterface;

interface PreferenceInfoTagInterface extends TagInterface
{

}
