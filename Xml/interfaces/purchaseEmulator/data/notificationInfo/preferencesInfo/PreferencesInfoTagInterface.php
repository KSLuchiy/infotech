<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\preferencesInfo;

use common\modules\Xml\base\tag\TagInterface;

interface PreferencesInfoTagInterface extends TagInterface
{

}
