<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\contractConditionsInfo\maxPriceInfo;

use common\modules\Xml\base\tag\TagInterface;

interface MaxPriceInfoTagInterface extends TagInterface
{

}
