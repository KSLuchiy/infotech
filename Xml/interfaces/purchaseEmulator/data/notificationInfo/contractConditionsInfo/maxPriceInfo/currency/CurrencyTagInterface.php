<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\contractConditionsInfo\maxPriceInfo\currency;

use common\modules\Xml\base\tag\TagInterface;

interface CurrencyTagInterface extends TagInterface
{

}
