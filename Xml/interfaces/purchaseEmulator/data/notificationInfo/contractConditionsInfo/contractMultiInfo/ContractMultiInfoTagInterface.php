<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\contractConditionsInfo\contractMultiInfo;

use common\modules\Xml\base\tag\TagInterface;

interface ContractMultiInfoTagInterface extends TagInterface
{

}
