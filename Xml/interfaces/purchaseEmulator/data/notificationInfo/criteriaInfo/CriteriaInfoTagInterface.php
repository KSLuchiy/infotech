<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\criteriaInfo;

use common\modules\Xml\base\tag\TagInterface;

interface CriteriaInfoTagInterface extends TagInterface
{

}
