<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo;

use common\modules\Xml\base\tag\TagInterface;

interface CriterionInfoTagInterface extends TagInterface
{
    public const COST_CRITERIA = 'costCriterionInfo';
    public const NON_COST_CRITERIA = 'qualitativeCriterionInfo';
}
