<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo\valueInfo;

use common\modules\Xml\base\tag\TagInterface;

interface ValueInfoTagInterface extends TagInterface
{

}
