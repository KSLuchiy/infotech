<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo\costCriterionInfo;

use common\modules\Xml\base\tag\TagInterface;

interface CostCriterionInfoTagInterface extends TagInterface
{

}
