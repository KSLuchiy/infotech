<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo\qualitativeCriterionInfo;

use common\modules\Xml\base\tag\TagInterface;

interface QualitativeCriterionInfoTagInterface extends TagInterface
{

}
