<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo\qualitativeCriterionInfo\indicatorsInfo\indicatorInfo;

use common\modules\Xml\base\tag\TagInterface;

interface IndicatorInfoTagInterface extends TagInterface
{

}
