<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\criteriaInfo\criterionInfo\qualitativeCriterionInfo\criterionInfo;

use common\modules\Xml\base\tag\TagInterface;

interface CriterionInfoTagInterface extends TagInterface
{

}
