<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\restrictionsInfo\restrictionInfo\restrictionsSt14\restrictionSt14\npaInfo;

use common\modules\Xml\base\tag\TagInterface;

interface NpaInfoTagInterface extends TagInterface
{

}
