<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\restrictionsInfo\restrictionInfo\restrictionsSt14\restrictionSt14\requirementsType;

use common\modules\Xml\base\tag\TagInterface;

interface RequirementsTypeTagInterface extends TagInterface
{

}
