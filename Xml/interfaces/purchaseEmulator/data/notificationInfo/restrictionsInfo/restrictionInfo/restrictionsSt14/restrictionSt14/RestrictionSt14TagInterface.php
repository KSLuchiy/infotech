<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\restrictionsInfo\restrictionInfo\restrictionsSt14\restrictionSt14;

use common\modules\Xml\base\tag\TagInterface;

interface RestrictionSt14TagInterface extends TagInterface
{

}
