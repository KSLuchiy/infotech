<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\restrictionsInfo\restrictionInfo\restrictionsSt14\restrictionSt14\requirementsType\requirementType;

use common\modules\Xml\base\tag\TagInterface;

interface RequirementTypeTagInterface extends TagInterface
{

}
