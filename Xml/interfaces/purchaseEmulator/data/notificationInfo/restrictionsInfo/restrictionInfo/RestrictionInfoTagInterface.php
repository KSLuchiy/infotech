<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\restrictionsInfo\restrictionInfo;

use common\modules\Xml\base\tag\TagInterface;

interface RestrictionInfoTagInterface extends TagInterface
{

}
