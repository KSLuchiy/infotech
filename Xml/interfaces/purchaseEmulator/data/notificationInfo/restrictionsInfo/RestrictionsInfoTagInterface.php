<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\notificationInfo\restrictionsInfo;

use common\modules\Xml\base\tag\TagInterface;

interface RestrictionsInfoTagInterface extends TagInterface
{

}
