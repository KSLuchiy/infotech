<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\purchaseResponsibleInfo\responsibleOrgInfo;

use common\modules\Xml\base\tag\TagInterface;

interface ResponsibleOrgInfoTagInterface extends TagInterface
{

}
