<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\purchaseResponsibleInfo\responsibleInfo;

use common\modules\Xml\base\tag\TagInterface;

interface ResponsibleInfoTagInterface extends TagInterface
{

}
