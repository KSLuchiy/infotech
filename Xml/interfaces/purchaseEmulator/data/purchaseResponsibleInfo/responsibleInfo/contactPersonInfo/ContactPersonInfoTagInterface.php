<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\purchaseResponsibleInfo\responsibleInfo\contactPersonInfo;

use common\modules\Xml\base\tag\TagInterface;

interface ContactPersonInfoTagInterface extends TagInterface
{

}
