<?php

namespace common\modules\Xml\interfaces\purchaseEmulator\data\purchaseResponsibleInfo;

use common\modules\Xml\base\tag\TagInterface;

interface PurchaseResponsibleInfoTagInterface extends TagInterface
{

}
