<?php

namespace common\modules\Xml\traits;

use DateInterval;
use DateTime;

trait DateTrait
{
    private function datePlusIntervalFormatted(DateTime $dateTime, string $interval, string $format = 'd.m.Y\TH:i:s'): string
    {
        $dateToBeFormatted = (clone $dateTime)->add(DateInterval::createFromDateString($interval));
        return $dateToBeFormatted->format($format);
    }
}
