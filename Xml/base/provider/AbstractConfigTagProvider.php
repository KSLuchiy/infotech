<?php

namespace common\modules\Xml\base\provider;

use common\Exception\InvalidArgumentException;
use common\modules\Xml\base\tag\TagInterface;
use Yii;
use yii\base\InvalidConfigException;

abstract class AbstractConfigTagProvider implements TagProviderInterface
{
    /**
     * @throws InvalidConfigException
     */
    public function getTag(string $tagInterface): TagInterface
    {
        if (!is_subclass_of($tagInterface, TagInterface::class, true)) {
            throw new InvalidArgumentException($tagInterface . ' должен расширять интерфейс ' . TagInterface::class);
        }

        $lastFoundImplementation = null;
        foreach ($this->getTagClasses() as $tagClass) {
            if (is_subclass_of($tagClass, $tagInterface, true)) {
                $lastFoundImplementation = $tagClass;
            }
        }

        if ($lastFoundImplementation === null) {
            throw new InvalidArgumentException('Не найден класс для интерфейса ' . $tagInterface);
        }

        /** @var TagInterface $tag */
        $tag = Yii::createObject($lastFoundImplementation, [$this]);
        return $tag;
    }

    /**
     * Получить список классов, реализующий интерфейсы тегов, которые участвуют в генерации
     * @return string[]
     */
    abstract protected function getTagClasses(): array;
}
