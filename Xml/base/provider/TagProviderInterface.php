<?php

namespace common\modules\Xml\base\provider;

use common\modules\Xml\base\tag\TagInterface;

interface TagProviderInterface
{
    /**
     * Получить объект тега, реализующий переданный интерфейс тега
     */
    public function getTag(string $tagInterface): TagInterface;
}
