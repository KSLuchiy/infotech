<?php

namespace common\modules\Xml\base\tag;

use common\Exception\InvalidArgumentException;
use common\modules\Xml\base\provider\TagProviderInterface;
use function gettype;

abstract class AbstractTag implements TagInterface
{
    /**
     * @var
     */
    protected $tagProvider;

    /**
     * @var mixed
     */
    protected $data;

    public function __construct(TagProviderInterface $tagProvider)
    {
        $this->tagProvider = $tagProvider;
    }

    public function setData($data): TagInterface
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return never
     */
    protected function throwInvalidDataTypeException($data, string $expectedType): void
    {
        $type = gettype($data);
        if ($type === 'object') {
            $type = get_class($data);
        }

        throw new InvalidArgumentException(sprintf('$data должна быть подклассом %s, но передан %s', $expectedType, $type));
    }
}
