<?php

namespace common\modules\Xml\base\tag;

use common\Exception\InvalidArgumentException;
use DOMDocument;
use DOMNode;
use Yii;

abstract class AbstractFromXmlTag extends AbstractTag
{
    public function generate(DOMDocument $domTree): DOMNode
    {
        $path = Yii::getAlias('@xmlTemplate') . '/' . $this->getPathToXml();
        if (!file_exists($path)) {
            throw new InvalidArgumentException('Не найден шаблон xml по пути ' . $path);
        }

        $templateDom = new DOMDocument();
        $templateDom->loadXML(file_get_contents($path));

        return $domTree->importNode($templateDom->documentElement, true);
    }

    abstract protected function getPathToXml(): string;
}
