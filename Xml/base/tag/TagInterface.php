<?php

namespace common\modules\Xml\base\tag;

use DOMDocument;
use DOMNode;

interface TagInterface
{

    /**
     * @param mixed $data - Данные, необходимые для генерации тега
     */
    public function setData($data): self;

    /**
     * Создаёт DOMNode. Если для генерации тега необходимы данные, то перед вызовом надо вызвать setData
     *
     * @throws \DOMException
     */
    public function generate(DOMDocument $domTree): DOMNode;
}
