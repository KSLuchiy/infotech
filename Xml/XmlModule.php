<?php

namespace common\modules\Xml;

use yii\base\Module;

class XmlModule extends Module
{
    public const MODULE_NAME = 'xml';

    public function __construct($id, $parent = null, $config = [])
    {
        parent::__construct($id, $parent, array_merge($config, require __DIR__ . '/config/config.php'));
    }
}
